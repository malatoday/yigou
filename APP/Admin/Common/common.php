<?php
	function isLogin(){
		if(session('app_id')!='YIGOU_ADMIN')return false;
		return true;
	}

	function getUserCount(){
		$User = D('User');
		return $User->getUserCount();
	}

	function getLoanCount(){
		$Loan = D('Loanmessage');
		return $Loan->getLoanCount();
	}

	function getLifeCount(){
		$Life = D('Lifeserve');
		return $Life->getLifeCount();
	}

	function getLunbotuPicInfoById($id){
		$Pic = D('Pic');
		return $Pic->getLunbotuPicInfoById($id);
	}

	function getCategoryNameById($id){
		$Category = D('Category');
		return $Category->getCategoryNameById($id);
	}

	function getCategoryPid($id){
		$Category = D('Category');
		return $Category->getPidById($id);
	}

	function selected($a, $b){
		if($a==$b)return "selected";
		return "";
	}

	function getAdPicInfoById($id){
		$Pic = D('Pic');
		return $Pic->getAdPicInfoById($id);
	}

	function getLunbotuCategory(){
		$Category = D('Category');
		return $Category->getLunbotuCategory();
	}

	function getAdCategory(){
		$Category = D('Category');
		return $Category->getAdCategory();
	}

	function getSellerCategory(){
		$Category = D('Category');
		return $Category->getSellerCategory();
	}

	function getDateForMe($timestamp){
		$data = getdate($timestamp);
		$result = $data['year'].'-'.$data['mon'].'-'.$data['mday'].' '.$data['hours'].':'.$data['minutes'].':'.$data['seconds'];
		return $result;
	}

	function getJustDate($timestamp){
		$data = getdate($timestamp);
		$result = $data['year'].'-'.$data['mon'].'-'.$data['mday'];
		return $result;
	}

	function getJustDateForH5($timestamp){
		return date('Y-m-d', $timestamp);
	}

	function getJustTime($timestamp){
		$data = getdate($timestamp);
		$result = $data['hours'].':'.$data['minutes'];
		return $result;
	}

	function getPicInfoById($id){
		$Pic = D('Pic');
		return $Pic->getPicInfoById($id);
	}

	function getAreaProvinceById($id){
		$Area = D('Area');
		return $Area->getAreaProvinceById($id);
	}

	function getAreaCityById($id){
		$Area = D('Area');
		return $Area->getAreaCityById($id);
	}

	function getAreaAreaById($id){
		$Area = D('Area');
		return $Area->getAreaAreaById($id);
	}
	
	function getAreaMoreById($id){
		$Area = D('Area');
		return $Area->getAreaMoreById($id);
	}

	function getRegionNameById($id){
		$Region = D('Region');
		$result = $Region->getRegionNameById($id);
		if($result=='')$result = '中国';
		return $result; 
	}

	function getRegionPid($id){
		$Region = D('Region');
		$result = $Region->getRegionPid($id);
		return $result;
	}

	function getSellerNameById($id){
		$Seller = M('Seller');
		return $Seller->where(array('id'=>$id))->getField('company_name');
	}

	function getBeType($typeid){
		switch($typeid){
			case C('BE_STATUS_ORDERING'):
				return '预约中';
			break;
			case C('BE_STATUS_ORDERSUCCESS'):
				return '预约成功';
			break;
			case C('BE_STATUS_ORDERFAILD'):
				return '预约失败';
			break;
			case C('BE_STATUS_ONLINE'):
				return '上线中';
			break;
			case C('BE_STATIS_OFFLINE'):
				return '产品下线';
			break;
			case C('BE_STATIS_CHECKOUT'):
				return '结账中';
			break;
			default:
				return 'ERROR';
			break;
		};
	}

	function getUsernameById($id){
		$User = D('User');
		return $User->getUsernameById($id);
	}
?>