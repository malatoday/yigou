<?php
return array(
	'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'yigou', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '', // 密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => 'yigou_', // 数据库表前缀 	
    'Lifeserve'	=>	1,
    'Loanmessage'	=>	2,

    'BE_STATUS_ORDERING'    =>  0,  // 预约中
    'BE_STATUS_ORDERSUCCESS'    =>  1,  // 预约成功
    'BE_STATUS_ORDERFAILD'  =>  2,  // 预约失败
    'BE_STATUS_ONLINE'  =>  3,  // 上线中
    'BE_STATIS_OFFLINE' =>  4,  // 产品下线
    'BE_STATIS_CHECKOUT'  =>  5,  // 结账中
    'SHOW_PAGE_TRACE' =>false
);
?>