<?php
	class LifeserveAction extends AdminAction{
		public function index(){
			$timeout = $this->_get('timeout');
			$Lifeserve = D('Lifeserve');
			$data = $Lifeserve->getAllLife();
			$res = array();
			if($timeout==2){
				foreach($data as $v){
					if((int)$v['limit_time']>time()){
						$res[] = $v;
					}
				}
			}else if($timeout==1){
				foreach($data as $v){
					if((int)$v['limit_time']<=time()){
						$res[] = $v;
					}
				}
			}else{
				$res = $data;
			}
			$this->assign('data', $res);
			$this->display();
		}

		private function checkDiscount($discount){
			$pattern = array('^[0-9]$', '^[0-9]\.[0-9]+$', '^[0-9]-[0-9]$', '^[0-9]\.[0-9]+-[0-9]\.[0-9]+$', '^[0-9]-[0-9]\.[0-9]+$', '^[0-9]\.[0-9]+-[0-9]$');
			foreach($pattern as $v){
				$temp = ereg($v, $discount);
				if($temp==1)return true;
			}
			return false;
		}

		public function newLifeserve(){
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->display();
		}

		public function editLifeserve(){
			$id = $this->_param('id');
			$Lifeserve = D('Lifeserve');
			$data = $Lifeserve->getLifeById($id);
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->assign('data', $data);
			$this->display();
		}

		public function removeLifeserve(){
			$id = $this->_param('id'); 
			$Lifeserve = D('Lifeserve');
			if(!$Lifeserve->removeLifeById($id))$this->error('删除失败');
			$this->success('删除成功', U('Lifeserve/index'));
		}

		public function newLifeserveHandle(){
			$data = array();
			if(!$this->checkDiscount($this->_post('discount')))$this->error('折扣格式不正确');
			if($this->_post('date')!=''){
				// 检测日期是否合法
				if(!$this->checkDate($this->_post('date')))
					$this->error('日期格式不合法');
			}
			if($this->_post('time')!=''){
				// 检测时间是否合法
				if(!$this->checkTime($this->_post('time')))
					$this->error('时间格式不合法');
			}
			
			$area['province'] = $this->_post('province');
			$area['city'] = $this->_post('city');
			$area['area'] = $this->_post('area');
			$area['more'] = $this->_post('more');
			$Area = D('Area');
			$areaId = $Area->addArea($area);
			if(!$areaId)$this->error('地址保存失败');
			// 处理文件上传
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				$this->error($upload->getErrorMsg());
			}else{
				$info =  $upload->getUploadFileInfo();
			}
			$Pic = D('Pic');
			$picId = $Pic->addPic($info[0]);
			$logoId = $Pic->addPic($info[1]);
			if(!$this->_post('company_name')!='')$this->error('公司名不可为空');
			if(!$this->_post('ad_title')!='')$this->error('广告语不可为空');
			if(!$this->_post('title')!='')$this->error('说明不可为空');
			if(!$this->_post('discount')!='')$this->error('折扣信息不可为空');
			$data['pic'] = $picId;
			$data['logo'] = $logoId;
			$data['title'] = $this->_post('title');
			$data['ad_title'] = $this->_post('ad_title');
			$data['company_name'] = $this->_post('company_name');
			$data['discount'] = $this->_post('discount');
			$arr1 = explode('-', $this->_post('date'));
			$arr2 = explode(':', $this->_post('time'));
			$data['limit_time'] = mktime($arr2[0], $arr2[1], $arr2[2], $arr1[1], $arr1[2], $arr1[0]);
			$data['area'] = $areaId;
			$data['category'] = $this->_post('category');
			$data['myorder'] = $this->_post('order')==''?-1:$this->_post('order');
			$data['index_order'] = $this->_post('index_order')==''?-1:$this->_post('index_order');
			$data['region'] = $this->_post('region');
			$Lifeserve = D('Lifeserve');
			if(!$Lifeserve->addLife($data))$this->error('生活服务信息添加失败');
			$this->success('生活服务信息添加成功', U('Lifeserve/index'));
		}

		private function checkDate($date){
			$arr = explode('-', $date);
			$data = array();
			foreach($arr as $v){
				$data[] = (int)$v;
			}
			if(checkdate($data[1], $data[2], $data[0]))return true;
			return false;
		}

		private function checkTime($time){
			$arr = explode(':', $time);
			$data = array();
			foreach($arr as $v){
				$data[] = (int)$v;
			}
			if($data[0]>=24 || $data[0]<0 || $data[1]>=60 || $data[1]<0)return false;
			return true;
		}

		public function editLifeserveHandle(){
			$data = array();
			if(!$this->checkDiscount($this->_post('discount')))$this->error('折扣格式不正确');
			$Lifeserve = D('Lifeserve');
			$last = $Lifeserve->getLifeById($this->_post('id'));
			
			if($this->_post('date')!=''){
				// 检测日期是否合法
				if(!$this->checkDate($this->_post('date')))
					$this->error('日期格式不合法');
			}
			if($this->_post('time')!=''){
				// 检测时间是否合法
				if(!$this->checkTime($this->_post('time')))
					$this->error('时间格式不合法');
			}
			
			
			$area['id'] = (int)$this->_post('area_id');
			$area['province'] = $this->_post('province');
			$area['city'] = $this->_post('city');
			$area['area'] = $this->_post('area');
			$area['more'] = $this->_post('more');
			$Area = D('Area');
			$lastArae = $Area->getAreaById($last['area']);
			if(!($area['province']==$lastArae['province'] && $area['city']==$lastArae['city'] && $area['area']==$lastArae['area'] && $area['more']==$lastArae['more'])){
				$areaId = $Area->where(array('id'=>$area['id']))->save($area);
				if(!$areaId)$this->error('地址更新失败');
			}
			// 处理文件上传
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				if(!$upload->getErrorMsg()=='没有选择上传文件')$this->error($upload->getErrorMsg());
			}else{
				$info = $upload->getUploadFileInfo();
			}
			$Pic = D('Pic');
			if(empty($info)){
				$info[0] = getPicInfoById($this->_post('pic_id'));
				$info[1] = getPicInfoById($this->_post('logo_id'));
			}
			$info[0]['id'] = $this->_post('pic_id');
			$info[1]['id'] = $this->_post('logo_id');
			$picId = $Pic->updatePic($info[0]);
			$logoId = $Pic->updatePic($info[1]);
			if(!$this->_post('company_name')!='')$this->error('公司名不可为空');
			if(!$this->_post('ad_title')!='')$this->error('广告语不可为空');
			if(!$this->_post('title')!='')$this->error('说明不可为空');
			$data['pic'] = $this->_post('pic_id');
			$data['logo'] = $this->_post('logo_id');
			$data['title'] = $this->_post('title');
			$data['ad_title'] = $this->_post('ad_title');
			$data['company_name'] = $this->_post('company_name');
			$arr1 = explode('-', $this->_post('date'));
			$arr2 = explode(':', $this->_post('time'));
			$data['limit_time'] = mktime($arr2[0], $arr2[1], $arr2[2], $arr1[1], $arr1[2], $arr1[0]);
			$data['area'] = $this->_post('area_id');
			$data['category'] = $this->_post('category');
			$data['myorder'] = $this->_post('order')==''?-1:$this->_post('order');
			$data['index_order'] = $this->_post('order')==''?-1:$this->_post('index_order');
			$data['id'] = (int)$this->_post('id');
			$data['region'] = $this->_post('region');
			$data['discount'] = $this->_post('discount');
			if($Lifeserve->save($data)===false)$this->error('生活服务信息修改失败,可能是您未对信息做任何变更');
			$this->success('生活服务信息修改成功', U('Lifeserve/index', array('timeout'=>2)));
		}
	}
?>