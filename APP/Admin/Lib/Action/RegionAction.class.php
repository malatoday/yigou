<?php
	class RegionAction extends AdminAction{
		public function index(){
			$Region = D('Region');
			$data = $Region->getAllData();
			$this->assign('data', $data);
			$this->display();
		}

		public function newRegion(){
			$Region = D('Region');
			$data = $Region->getAllPids();
			$this->assign('data', $data);
			$this->display();
		}

		public function editRegion(){
			$id = $this->_param('id');
			$Region = D('Region');
			$data = $Region->getAllData();
			$this->assign('id', $id);
			$this->assign('data', $data);
			$this->display();
		}

		public function newRegionHandle(){
			if($this->_post('name')=='')$this->error('区域名称不可为空');
			$data = array();
			$data['name'] = $this->_post('name');
			$data['pid'] = $this->_post('pid');
			$Region = D('Region');
			if(!$Region->add($data))$this->error('新增失败');
			$this->success('新增成功', U('Region/index'));
		}

		public function editRegionHandle(){
			$id = $this->_param('id');
			if($this->_post('name')=='')$this->error('名称不可为空');
			$Region = D('Region');
			$data = array();
			$data['id'] = $id;
			$data['name'] = $this->_post('name');
			$data['pid'] = $this->_post('pid');
			if(!$Region->save($data))$this->error('保存失败');
			$this->success('保存成功', U('Region/index'));
		}

		public function removeRegion(){
			$id = $this->_param('id');
			$Region = D('Region');
			$Region->removeById($id);
			$this->success('删除成功', U('Region/index'));
		}

	}
?>