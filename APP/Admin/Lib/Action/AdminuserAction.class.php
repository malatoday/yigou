<?php
	class AdminuserAction extends AdminAction{
		public function index(){
			$this->display();
		}

		public function changePassword(){
			$this->display();
		}

		public function changePasswordHandle(){
			if($this->_post('old')=='')$this->error('你没有输入旧密码！');
			if($this->_post('new')=='')$this->error('你没有输入新的密码！');
			if($this->_post('repeat')=='')$this->error('请重复输入一次您的新密码');
			if($this->_post('new')!=$this->_post('repeat'))$this->_post('两次输入的密码不一致');
			$Adminuser = D('Adminuser');
			if(md5($this->_post('old'))!=$Adminuser->getPassword())$this->error('密码输入有误！');
			if(!$Adminuser->updatePassword($this->_post('new')))$this->error('密码更新失败');
			$this->success('密码更新成功', U('Index/index'));
		}
	}
?>