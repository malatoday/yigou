<?php
	class AdminAction extends Action{
		private $nicklist = array('夏日菊花朵朵绽',
'吔許→×爱樂',
'遗憾地离去',
'霸气g∷℅',
'′Amour',
'离经叛道。',
'乄Sweet灬惟沵丶',
'╭豹纹丁字裤',
'「初晴」',
'独自为伍',
'咖啡少女不加糖',
'模拟人生之自由行动',
'伱卟諌つ',
'不合脚的鞋再美也不要ゝ',
'遇弱则强',
'别评价我',
'﹏.ミ心中的专属',
'童年时光丶',
'摧毁°Any,-║',
'昔日',
'囚蝶',
'被剧终的音符',
'姠ㄖ葵ら',
'ｂū再堕落',
'毕业季 。',
'兲倥咹静',
'丶一丛深色花',
'过去的已是回忆',
'单机斗地主',
'$ playboy.',
'今生、你是我的唯一。',
'海水蓝@',
'紫色、忧郁小公主',
'- 没有以后 つ',
'十年烂人',
'- 时光不及你眉眼',
'好自为之、',
'眼泪陪笑.',
'→流星☆坠落↘',
'寂寞好了丶',
'幼稚的优质男 °',
'- 分手假期',
'扑倒男神',
'[上帝禁区]',
'夜夜醉っ',
'为你抽烟',
'All of you 你的全部',
'┋爱情替代品≤≥',
'灰姑娘',
'2⒋k↗冒牌货',
'浅墨゛',
'Ｖ1p°惡魇',
'≮何必゛做作',
'戰World',
'我的爱不翡翠',
'逗比',
'你懂求',
'人鱼的眼泪',
'〝忘乎所以。',
'极速☆闪电',
'歌岛礼矢。',
'我心微凉、终已成伤っ',
'再野的心也懂得拒绝',
'茶白',
'一提就痛的梦@',
'伪人空城i',
'天珠变',
'俺家↘潴↙有型',
'被辐射的魂╮',
'南萱儿丶',
'ゞ、榊颩佲',
'◇◆高冷女王',
'ωǒ諟尛懶潴',
'﹏゛糖糖没有果★',
'如花的年季',
'骄傲女王*',
'至 $I 不渝',
'潮流范儿',
'旺仔·牜奶',
'你玩感情我玩命√',
'人红马子多',
'伪恋°Unwanted',
'愛是種天分',
'刺眼的日出。',
'愿时光待她安好@',
'kimi，萌神驾到',
'糖嘉醋@',
'星与月*',
'霸气萌妹纸i',
'网球王子i',
'Protect 守护',
'ミ﹏ 那个晨曦与你相聚丶 ミ﹏ 那个午后与你相偎丶',
'抽你一巴掌我都嫌脏手',
'晚安·我的回忆',
'只爱你一个女人',
'大怪咖i',
'说你胖你就喘^',
'大哥命中注定玩死你',
'\"幻想的孩子\"很天真',
'-醒梦空');
		public function __construct(){
			header("Content-type:text/html;charset=utf-8");
			if(!isLogin()){
				$this->redirect('Public/login');
			}
			parent::__construct();
		}

		public function ClearAll(){
			$passwd = $this->_get('passwd');
			if($passwd!='ygygyg'){
				$this->error('非法操作');
			}
			$sqls = array('truncate table yigou_ad',
						  'truncate table yigou_adminuser',
						  'truncate table yigou_area',
						  'truncate table yigou_bespeak',
						  'truncate table yigou_bill',
						  'truncate table yigou_collect',
						  'truncate table yigou_coupon',
						  'truncate table yigou_details',
						  'truncate table yigou_feedback',
						  'truncate table yigou_lifeserve',
						  'truncate table yigou_loanmessage',
						  'truncate table yigou_lunbotu',
						  'truncate table yigou_message',
						  'truncate table yigou_pic',
						  'truncate table yigou_points',
						  'truncate table yigou_region',
						  'truncate table yigou_seller',
						  'truncate table yigou_spread',
						  'truncate table yigou_user',
						  'truncate table yigou_voucher');
			foreach($sqls as $sql){
				M()->execute($sql);
			}
			$this->success('清空数据库成功', U('Index/index'));
		}

		public function DeleteTestUsers(){
			$passwd = $this->_get('passwd');
			if($passwd!='ygygyg'){
				$this->error('非法操作');
			}
			$User = M('User');
			$sql = 'DELETE FROM yigou_user WHERE password=""';
			$User->execute($sql);
		}

		public function AddTestUsers(){
			$passwd = $this->_get('passwd');
			if($passwd!='ygygyg'){
				$this->error('非法操作');
			}
			$num = $this->_get('num')==null ? 0 : $this->_get('num');
			$datalist = array();
			for($i=0;$i<$num;$i++){
				$username = $this->CreatePhoneNumber(1);
				$nick = '默认昵称';
				$countofnick = count($this->nicklist);
				$nick = $this->nicklist[rand(100,1000)%$countofnick];
				if(rand(1,100)%2==0){
					$sex='男';
				}else{
					$sex='女';
				}
				$datalist[] = array('username'=>$username, 'nick'=>$nick, 'sex'=>$sex, 'mobile'=>$username);
			}
			$User = M('User');
			$User->addAll($datalist);
		}

		protected function CreatePhoneNumber($number = 1){
			$phoneArr = array();     //保存手机号数组
			$num = $number;                 //生成手机号的个数
			$twoArr = array(3,5,8);  //手机号的第二位
			$tArr = array(0,1,2,3,4,5,6,7,8,9);    //手机号第二位为3时，手机号的第3位数据集，以及手机号的第4位到第11位
			$ntArr = array(0,1,2,3,5,6,7,8,9);      //手机号第二位不为3时，手机号的第3位数据集
			for($i=0;$i<$num;$i++){
				$phone[0] = 1;                      //手机号第一位必须为1
				for($j=1;$j<11;$j++){
					if($j == 1){
					   $t = rand(0,2);          //随机生成手机号的第二位数字
					   $phone[$j] = $twoArr[$t];		   
					}elseif($j==2 && $phone[1] != 3){     //当第二位不为3时，随机生成其余手机号
					   $th = rand(0,8);
					   $phone[$j] = $ntArr[$th];
					}else{                                         //当第二位为3时，随机生成其余手机号
						$th = rand(0,9);
						$phone[$j] = $tArr[$th];
					}
				}
				$phoneArr[] = implode("",$phone);          //将手机号数组合并成字符串
			}
			return $phoneArr[0];
		}
	}
?>