<?php
	class PublicAction extends Action{
		public function login(){
			$AdminUser = D('Adminuser');
			if(!$AdminUser->isAdminUserExist())$this->redirect('Public/register');
			$this->display();
		}

		public function checkLogin(){
			if($this->_post('username')=='')$this->error('账号错误');
			if($this->_post('password')=='')$this->error('密码为空！');
			$AdminUser = D('Adminuser');
			if(!$AdminUser->checkUser($this->_post('username'), md5($this->_post('password')))){
				$this->error('验证错误');
			}
			session('app_id', 'YIGOU_ADMIN');
			session('username', $this->_post('username'));
			$this->success('登陆成功！', U('Index/index'));
		}

		public function register(){
			$AdminUser = D('Adminuser');
			if($AdminUser->isAdminUserExist())$this->redirect('Public/login');
			$this->display();
		}

		public function registerHandle(){
			$AdminUser = D('Adminuser');
			if(!$AdminUser->addUser($this->_post('username'), md5($this->_post('password')))){
				$AdminUser->clear();
				$this->error('初始化失败！');
			}
			$this->success('初始化成功！', U('Public/login'));
		}

		public function loginOut(){
			session(null);
			$this->success('成功登出');
		}
	}
?>