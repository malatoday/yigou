<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends AdminAction {
    public function index(){
		$this->display();	
	}

	public function test(){
		$this->display();
	}

	public function feedback(){
		$Feedback = M('Feedback');
		$data = $Feedback->select();
		$this->assign('data', $data);
		$this->display();
	}

	public function showUsers(){
		$keyword = $this->_get('keyword');
		//$keys = explode(' ', $keyword);
		$sql = "select * from yigou_user where(username like '%$keyword%' OR 
			nick like '%$keyword%' OR 
			mail like '%$keyword%' OR 
			mobile like '%$keyword%' OR 
			alipayaccount like '%$keyword%' OR 
			alipayname like '%$keyword%' )";
		$User = D('User');
		if(empty($keyword)){
			$data = $User->select();
		}else{
			$data = $User->query($sql);
		}
		$this->assign('data',$data);
		$this->display();
	}

	public function coupon(){
		$Coupon = M('Coupon');
		$data = $Coupon->select();
		$this->assign('data', $data);
		$this->display();
	}

	public function voucher(){
		$Voucher = M('Voucher');
		$data = $Voucher->select();
		$this->assign('data', $data);
		$this->display();
	}

	public function bill(){
		$keyword = $this->_get('keyword');
		$sql = "select * from yigou_seller where(company_name like '%$keyword%' OR 
			company_tel like '%$keyword%' OR 
			name like '%$keyword%' OR 
			tel like '%$keyword%' OR 
			username like '%$keyword%' )";
		$sql2uid = "SELECT * FROM yigou_user WHERE(username LIKE '%$keyword%')";
		$Seller = M('Seller');
		$User = M('User');
		$Bill = M('Bill');
		if(empty($keyword)){
			$data = $Bill->select();
		}else{
			$sellers = $Seller->query($sql);
			$arr = array();
			foreach($sellers as $v){
				$arr[] = $v['id'];
			}
			if(!empty($arr)){
				$map['seller'] = array('in', $arr);
				$data1 = $Bill->where($map)->select();
			}else{
				$data1 = array();
			}
			$users = $User->query($sql2uid);
			$arr1 = array();
			foreach($users as $v){
				$arr1[] = $v['id'];
			}
			if(!empty($arr1)){
				$map1['uid'] = array('in', $arr1);
				$data2 = $Bill->where($map1)->select();
				$data = array_merge_recursive($data1, $data2);
			}else{
				$data = $data1;
			}
		}
		$this->assign('data', $data);
		$this->display();
	}
}