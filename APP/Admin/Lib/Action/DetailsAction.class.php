<?php
	class DetailsAction extends AdminAction{
		public function index(){
			$type = $this->_param('type');
			$id = $this->_param('id');
			if($type==null || $id==null){
				$this->error('不合法的参数');
			}
			$Details = M('Details');
			$info = $Details->where(array('type'=>$type, 'lid'=>$id))->find();
			if($info==null){
				$this->redirect('Details/add', array('type'=>$type, 'id'=>$id), 0);
			}elseif($info===false){
				$this->error('系统错误，请稍后再试');
			}else{
				$this->redirect('Details/edit', array('type'=>$type, 'id'=>$id), 0);
			}
		}

		public function edit(){
			$type = $this->_param('type');
			$id = $this->_param('id');
			$Details = M('Details');
			$info = $Details->where(array('type'=>$type, 'lid'=>$id))->find();
			if($type==1){
				$typename='生活服务类';
				$actionname  = 'Lifeserve';
			}
			if($type==2){
				$typename='贷款信息类';
				$actionname  = 'Loanmessage';
			}
			$Seller = D('Seller');
			$sellers = $Seller->getAllSeller();
			$this->assign('info', $info);
			$this->assign('id', $id);
			$this->assign('type', $type);
			$this->assign('typename', $typename);
			$this->assign('actionname', $actionname);
			$this->assign('seller', $sellers);
			$this->display();
		}

		public function add(){
			$type = $this->_param('type');
			if($type==1){
				$typename='生活服务类';
				$actionname  = 'Lifeserve';
			}
			if($type==2){
				$typename='贷款信息类';
				$actionname  = 'Loanmessage';
			}
			$Seller = D('Seller');
			$sellers = $Seller->getAllSeller();
			$id = $this->_param('id');
			$this->assign('type', $type);
			$this->assign('typename', $typename);
			$this->assign('actionname', $actionname);
			$this->assign('id', $id);
			$this->assign('seller', $sellers);
			$this->display();
		}

		public function addHandle(){
			$id = $this->_param('id');
			$type = $this->_param('type');
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				$this->error($upload->getErrorMsg());
			}else{
				$info = $upload->getUploadFileInfo();
			}
			$Pic = D('Pic');
			$picId = $Pic->addPic($info[0]);
			$data = array();
			if($this->_post('daijinquan')){
				$data['daijinquan'] = 1;
			}else{
				$data['daijinquan'] = 0;
			}
			if($this->_post('youhuiquan')){
				$data['youhuiquan'] = 1;
			}else{
				$data['youhuiquan'] = 0;
			}
			if($this->_post('navbar')){
				$data['navbar'] = 1;
			}else{
				$data['navbar'] = 0;
			}
			$data['pic'] = $picId;
			$data['lid'] = $id;
			$data['type'] = $type;
			$data['seller'] = $this->_post('seller');
			$data['discount'] = $this->_post('discount');
			$Details = M('Details');
			if(!$Details->add($data))$this->error('添加失败');
			if($type==1){
				$this->success('添加成功', U('Lifeserve/index'));
			}else{
				$this->success('添加成功', U('Loanmessage/index'));
			}
		}

		public function editHandle(){
			$id = $this->_param('id');
			$type = $this->_param('type');
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');
			$upload->savePath =  'Public/pic/';
			$Pic = D('Pic');
			$Details = D('Details');
			$picId = $Details->where(array('lid'=>$id, 'type'=>$type))->getField('pic');
			if(!$upload->upload()) {
				if($upload->getErrorMsg()!='没有选择上传文件'){
					$this->error($upload->getErrorMsg());
				}
			}else{
				$info = $upload->getUploadFileInfo();
				$info[0]['id'] = $picId;
				$res = $Pic->updatePic($info[0]);
				if($res===false)$this->error('图片数据更新失败');
			}
			$data = array();
			if($this->_post('daijinquan')){
				$data['daijinquan'] = 1;
			}else{
				$data['daijinquan'] = 0;
			}
			if($this->_post('youhuiquan')){
				$data['youhuiquan'] = 1;
			}else{
				$data['youhuiquan'] = 0;
			}
			if($this->_post('navbar')){
				$data['navbar'] = 1;
			}else{
				$data['navbar'] = 0;
			}
			$data['pic'] = $picId;
			$data['lid'] = $id;
			$did = $Details->where(array('lid'=>$id, 'type'=>$type))->getField('id');
			$data['type'] = $type;
			$data['id'] = $did;
			if($Details->save($data)===false)$this->error('更新失败');
			if($type==1){
				$this->success('更新成功', U('Lifeserve/index'));
			}else{
				$this->success('更新成功', U('Loanmessage/index'));
			}
		}
	}
?>