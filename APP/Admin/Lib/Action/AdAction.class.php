<?php
	class AdAction extends AdminAction{
		public function index(){
			$Ad = D('Ad');
			$data = $Ad->select();
			$this->assign('data', $data);
			$this->display();
		}

		public function newAd(){
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->display();
		}

		public function newAdHandle(){
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				$this->error($upload->getErrorMsg());
			}else{
				$info =  $upload->getUploadFileInfo();
			}
			// 保存表单数据 包括附件数据
			$Ad = D("Ad");
			$Pic = D('Pic');
			$picId = $Pic->addPic($info[0]);
			if($picId == false)$this->error('上传图片失败');
			$data = array();
			$data['pic'] = $picId;
			$data['title'] = $this->_post('title');
			$data['mshow'] = $this->_post('show');
			$data['link'] = $this->_post('link')==''?'#':$this->_post('link');
			$data['target'] = $this->_post('target');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			if(!$Ad->add($data))$this->error('轮播图保存失败');
			$this->success('数据保存成功！', U('Ad/index'));
		}

		public function removeAd(){
			$id = $this->_param('id');
			if($id == null)$this->error('参数无效');
			$Ad = D('Ad');
			if($Ad->removeOneById($id) == false)$this->error('删除失败');
			$this->success('删除成功', U('Ad/index'));
		}

		public function editAd(){
			$id = $this->_param('id');
			if($id == null)$this->error('参数无效');
			$Ad = D('Ad');
			$data = $Ad->getAdById($id);
			$imgInfo = getAdPicInfoById($data['pic']);
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->assign('data', $data);
			$this->assign('imgInfo', $imgInfo);
			$this->display();
		}

		public function editAdHandle(){
			import('ORG.Net.UploadFile');
			$data = array();
			$Ad = D("Ad");
			$Pic = D('Pic');
			$last = $Ad->getAdById($this->_post('id'));
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				if(!$upload->getErrorMsg()=='没有选择上传文件')$this->error($upload->getErrorMsg());
					
			}else{
				$info =  $upload->getUploadFileInfo();
			}
			$picId = $Pic->addPic($info[0]);
			$link = $this->_post('link');
			if($picId == false)$picId = $last['pic'];
			$data['id'] = (int)$this->_post('id');
			$data['pic'] = (int)$picId;
			$data['title'] = $this->_post('title')==''?$last['title']:$this->_post('title');
			$data['mshow'] = $this->_post('show')==''?(int)$last['show']:(int)$this->_post('show');
			$data['link'] = $link==''?$last['link']:$link;
			$data['target'] = $this->_post('target');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			if($Ad->save($data)===false)$this->error('保存失败');
			$this->success('更新成功', U('Ad/index'));
		}
	}
?>