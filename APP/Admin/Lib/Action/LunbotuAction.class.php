<?php
	class LunbotuAction extends AdminAction{
		public function index(){
			$Lunbotu = D('Lunbotu');
			$data = $Lunbotu->select();
			$this->assign('data', $data);
			$this->display();
		}

		public function newLunbotu(){
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->display();
		}

		public function newLunbotuHandle(){
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				$this->error($upload->getErrorMsg());
			}else{
				$info =  $upload->getUploadFileInfo();
			}
			// 保存表单数据 包括附件数据
			$Lunbotu = D("Lunbotu");
			$Pic = D('Pic');
			$picId = $Pic->addPic($info[0]);
			if($picId == false)$this->error('上传图片失败');
			$data = array();
			$data['pic'] = $picId;
			$data['title'] = $this->_post('title');
			$data['myorder'] = $this->_post('order');
			$data['link'] = $this->_post('link');
			$data['target'] = $this->_post('target');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			if(!$Lunbotu->add($data))$this->error('轮播图保存失败');
			$this->success('数据保存成功！', U('Lunbotu/index'));
		}

		public function removeLunbotu(){
			$id = $this->_param('id');
			if($id == null)$this->error('参数无效');
			$Lunbotu = D('Lunbotu');
			if($Lunbotu->removeOneById($id) == false)$this->error('删除失败');
			$this->success('删除成功', U('Lunbotu/index'));
		}

		public function editLunbotu(){
			$id = $this->_param('id');
			if($id == null)$this->error('参数无效');
			$Lunbotu = D('Lunbotu');
			$data = $Lunbotu->getLunbotuById($id);
			$imgInfo = getLunbotuPicInfoById($data['pic']);
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->assign('data', $data);
			$this->assign('imgInfo', $imgInfo);
			$this->display();
		}

		public function editLunbotuHandle(){
			import('ORG.Net.UploadFile');
			$data = array();
			$Lunbotu = D("Lunbotu");
			$Pic = D('Pic');
			$last = $Lunbotu->getLunbotuById($this->_post('id'));
			$upload = new UploadFile();
			$upload->maxSize  = 3145728 ;// 设置附件上传大小
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
			$upload->savePath =  'Public/pic/';
			if(!$upload->upload()) {
				if(!$upload->getErrorMsg()=='没有选择上传文件')$this->error($upload->getErrorMsg());	
			}else{
				$info =  $upload->getUploadFileInfo();
			}
			$picId = $Pic->addPic($info[0]);
			if($picId == false)$picId = $last['pic'];
			$data['id'] = (int)$this->_post('id');
			$data['pic'] = (int)$picId;
			$data['title'] = $this->_post('title')==''?$last['title']:$this->_post('title');
			$data['myorder'] = $this->_post('order')==''?(int)$last['order']:(int)$this->_post('order');
			$data['link'] = $this->_post('link')==''?$last['link']:$this->_post('link');
			$data['target'] = $this->_post('target');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			if(!$Lunbotu->save($data))$this->error('轮播图保存失败');
			$this->success('更新成功', U('Lunbotu/index'));
		}
	}
?>