<?php
class BespeakAction extends AdminAction{
	public function index(){
		$status = $this->_get('status');
		$Bespeak = M('Bespeak');
		import('ORG.Util.Page');// 导入分页类
		if($status!=null){
			$count = $Bespeak->where(array('status'=>$status))->count();
			$Page       = new Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数
			$show       = $Page->show();// 分页显示输出
			$list = $Bespeak->where(array('status'=>$status))->limit($Page->firstRow.','.$Page->listRows)->select();
		}else{
			$count = $Bespeak->count();
			$Page       = new Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数
			$show       = $Page->show();// 分页显示输出
			$list = $Bespeak->limit($Page->firstRow.','.$Page->listRows)->select();
		}
		$this->assign('list',$list);// 赋值数据集
		$this->assign('page',$show);// 赋值分页输出
		//$this->assign('data', $data);
		$this->display();
	}

	public function check(){
		$id = $this->_param('id');
		$Bespeak = M('Bespeak');
		$data = $Bespeak->where(array('id'=>$id))->find();
		$this->assign('data', $data);
		$this->display();
	}

	public function setStatus(){
		$id = $this->_param('id');
		$status = $this->_param('status');
		$Bespeak = M('Bespeak');
		if($Bespeak->where(array('id'=>$id))->setField('status', $status)===false){
			$this->error('设置失败请重试');
		}
		if($status == C('BE_STATUS_ORDERSUCCESS')){
			$Bespeak->where(array('id'=>$id))->setField('pass_time', time());
		}
		$this->success('设置成功');
	}

	public function setOnlineTime(){
		dump($_POST);
		$id = $this->_post('id');
		if($this->_post('date')!=''){
			// 检测日期是否合法
			if(!$this->checkDate($this->_post('date')))
				$this->error('日期格式不合法');
		}
		if($this->_post('time')!=''){
			// 检测时间是否合法
			if(!$this->checkTime($this->_post('time')))
				$this->error('时间格式不合法');
		}
		$arr1 = explode('-', $this->_post('date'));
		$arr2 = explode(':', $this->_post('time'));
		$online_time = mktime($arr2[0], $arr2[1], $arr2[2], $arr1[1], $arr1[2], $arr1[0]);
		$Bespeak = M('Bespeak');
		if($Bespeak->where(array('id'=>$id))->setField('online_time', $online_time)===false){
			$this->error('上线时间提交错误');
		}
		$this->success('上线时间提交成功');

	}

	private function checkDate($date){
		$arr = explode('-', $date);
		$data = array();
		foreach($arr as $v){
			$data[] = (int)$v;
		}
		if(checkdate($data[1], $data[2], $data[0]))return true;
		return false;
	}

	private function checkTime($time){
		$arr = explode(':', $time);
		$data = array();
		foreach($arr as $v){
			$data[] = (int)$v;
		}
		if($data[0]>=24 || $data[0]<0 || $data[1]>=60 || $data[1]<0)return false;
		return true;
	}
}