<?php
	class SellerAction extends AdminAction{
		public function index(){
			$cid = $this->_get('category');
			$Seller = D('Seller');
			if(!empty($cid)){
				$data = $Seller->where(array('category'=>$cid))->select();
			}else{
				$data = $Seller->getAllSeller();
			}
			$Category = D('Category');
			$category = $Category->getSellerCategory();
			$this->assign('data', $data);
			$this->assign('category', $category);
			$this->display();
		}

		public function newSeller(){
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->display();
		}

		public function newSellerHandle(){
			if($this->_post('company_name')=='')$this->error('公司名称不可为空');
			if($this->_post('company_tel')=='')$this->error('公司电话不可为空');
			if($this->_post('name')=='')$this->error('负责人姓名不可为空');
			if($this->_post('tel')=='')$this->error('负责人手机号码不可为空');
			if($this->_post('username')=='')$this->error('登陆名不可为空');
			if($this->_post('password')=='')$this->error('密码不可为空');
			if($this->_post('password')!=$this->_post('repeat'))$this->error('两次输入的密码不一致');
			if(!ereg('^[0-9]{11}$', $this->_post('tel')))$this->error('手机号码格式不正确');
			if($this->_post('category')=='')$this->error('服务类别不可为空');
			$area['province'] = $this->_post('province');
			$area['city'] = $this->_post('city');
			$area['area'] = $this->_post('area');
			$area['more'] = $this->_post('more');
			$Area = D('Area');
			$areaId = $Area->addArea($area);
			if(!$areaId)$this->error('地址保存失败');
			$data = array();
			$data['username'] = $this->_post('username');
			$data['password'] = md5($this->_post('password'));
			$data['company_name'] = $this->_post('company_name');
			$data['company_tel'] = $this->_post('company_tel');
			$data['name'] = $this->_post('name');
			$data['tel'] = $this->_post('tel');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			$data['area'] = $areaId;
			$Seller = D('Seller');
			if($Seller->where(array('username'=>$this->_post('username')))->find()!=null){
				$this->error('商家已经被注册');
			}
			if(!$Seller->add($data))$this->error('商户添加失败');
			$this->success('商户添加成功', U('Seller/index'));
		}

		public function editSeller(){
			$id = $this->_param('id');
			$Seller = D('Seller');
			$data = $Seller->getSellerById($id);
			$Region = D('Region');
			$region = $Region->getAllData();
			$this->assign('region', $region);
			$this->assign('data', $data);
			$this->display();
		}

		public function editSellerHandle(){
			$Seller = D('Seller');
			$res = $Seller->getSellerById($this->_post('id'));
			if($this->_post('company_name')=='')$this->error('公司名称不可为空');
			if($this->_post('company_tel')=='')$this->error('公司电话不可为空');
			if($this->_post('name')=='')$this->error('负责人姓名不可为空');
			if($this->_post('tel')=='')$this->error('负责人手机号码不可为空');
			if(!ereg('^[0-9]{11}$', $this->_post('tel')))$this->error('手机号码格式不正确');
			if($this->_post('category')=='')$this->error('服务类别不可为空');
			$area['province'] = $this->_post('province');
			$area['city'] = $this->_post('city');
			$area['area'] = $this->_post('area');
			$area['more'] = $this->_post('more');
			$area['id'] = $res['area'];
			$Area = D('Area');
			$areaId = $Area->updateArea($area);
			if($areaId===false)$this->error('地址保存失败');
			$data = array();
			$data['company_name'] = $this->_post('company_name');
			$data['company_tel'] = $this->_post('company_tel');
			$data['name'] = $this->_post('name');
			$data['tel'] = $this->_post('tel');
			$data['category'] = $this->_post('category');
			$data['region'] = $this->_post('region');
			$data['area'] = $this->_post('area_id');
			$data['id'] = $this->_post('id');
			
			if($this->_post('username')==''){
				$data['username'] = $res['username'];
			}else{
				$data['username'] = $this->_post('username');
			}
			if($this->_post('password')==''){
				$data['password'] = $res['password'];
			}else{
				if($this->_post('password')==$this->_post('repeat')){
					$data['password'] = md5($this->_post('password'));
				}else{
					$this->error('两次输入的密码不一致');
				}
			}
			if($Seller->updateSeller($data)===false)$this->error('商户修改失败');
			$this->success('商户修改成功', U('Seller/index'));
		}

		public function removeSeller(){
			$Seller = D('Seller');
			if($Seller->where(array('id'=>$this->_param('id')))->delete()===false){
				$this->error('删除失败');
			}
			$this->success('删除成功');
		}

		public function showBill(){
			$id = $this->_param('sid');
			$Bill = M('Bill');
			$data = $Bill->where(array('seller'=>$id))->order('id desc')->select();
			if($this->_get('byuser')!=null){
				$data = $Bill->where(array('seller'=>$id))->order('uid')->select();
			}
			if($this->_get('bypre')!=null){
				$data = $Bill->where(array('seller'=>$id))->order('pre')->select();
			}
			if($this->_get('bysuf')!=null){
				$data = $Bill->where(array('seller'=>$id))->order('suf')->select();
			}
			$this->assign('data',$data);
			$this->assign('sid', $id);
			$this->display();
		}
	}
?>