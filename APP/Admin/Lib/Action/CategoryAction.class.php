<?php
	class CategoryAction extends AdminAction{
		private $systemIntel = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22); 

		public function index(){
			$Category = D('Category');
			$knowCategory = $Category->readAllCategorys();
			$this->assign('data', $knowCategory);
			$this->display();
		}

		public function newCategory(){
			$Category = D('Category');
			$knowCategory = $Category->readAllCategorys();
			$this->assign('knowCategory', $knowCategory);
			$this->display();
		}

		public function newCategoryHandle(){
			if($this->_post('name')=='')$this->error('类别名称没有填写');
			$Category = D('Category');
			$result = $Category->addACategory($this->_post('name'), $this->_post('pid'));
			if(!$result)$this->error('类别添加失败');
			$this->success('添加成功', U('Category/index'));
		}

		public function editCategory(){
			$id = $this->_param('id');
			$Category = D('Category');
			$knowCategory = $Category->readAllCategorys();
			$this->assign('id', $id);
			$this->assign('knowCategory', $knowCategory);
			$this->display();
		}

		public function editCategoryHandle(){
			$Category = D('Category');
			$data = array();
			$data['id'] = $this->_post('id');
			$data['name'] = $this->_post('name')==''?$Category->getCategoryNameById($this->_post('id')):$this->_post('name');
			if(in_array($this->_post('id'), $this->systemIntel)){
				$data['pid'] = $Category->getPidById($this->_post('id'));
			}else{
				$data['pid'] = $this->_post('pid');
			}
			if($Category->save($data)==false)$this->error('保存失败');
			$this->success('保存成功', U('Category/index'));
		}

		public function removeCategory(){
			$id = $this->_param('id');
			if(in_array($id, $this->systemIntel))$this->error('系统内置分类，无法删除');
			$Category = D('Category');
			if($Category->removeCategoryById($id))$this->error('删除失败');
			$this->success('删除成功', U('Category/index'));
		}
	}
?>