<?php
	class UserModel extends Model{
		public function getUserCount(){
			$data = $this->select();
			return count($data);
		}

		public function getUsernameById($id){
			return $this->where(array('id'=>$id))->getField('username');
		}
	}
?>