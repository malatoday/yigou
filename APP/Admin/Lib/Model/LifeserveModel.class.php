<?php
	class LifeserveModel extends Model{
		public function removeLifeById($id){
			return $this->where(array('id'=>$id))->delete();
		}

		public function getLifeById($id){
			return $this->where(array('id'=>$id))->find();
		}

		public function addLife($data){
			return $this->add($data);
		}

		public function getAllLife(){
			return $this->order('limit_time desc')->select();
		}

		public function updateLife($data){
			return $this->save($data);
		}

		public function getLifeCount(){
			$result = $this->select();
			return count($result);
		}
	}
?>