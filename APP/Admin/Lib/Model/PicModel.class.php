<?php
	class PicModel extends Model{
		public function getLunbotuPicInfoById($id){
			$result = $this->where(array('id'=>$id))->find();
			if($result == false)return false;
			if($result == null){
				$result['savepath'] = 'Public/pic/';
				$result['savename'] = 'default_lunbotu.png';
			}
			return $result;
		}

		public function getAdPicInfoById($id){
			$result = $this->where(array('id'=>$id))->find();
			if($result == false)return false;
			if($result == null){
				$result['savepath'] = 'Public/pic/';
				$result['savename'] = 'default_ad.png';
			}
			return $result;
		}

		public function getPicInfoById($id){
			$result = $this->where(array('id'=>$id))->find();
			if($result == null){
				$result['savepath'] = 'Public/pic/';
				$result['savename'] = 'default_pic.png';
			}
			return $result;
		}

		public function addPic($dataArr){
			if(empty($dataArr))return false;
			$result = $this->add($dataArr);
			if($result == false)return false;
			return $result;
		}

		public function updatePic($dataArr){
			if(empty($dataArr))return false;
			$result = $this->save($dataArr);
			if($result == false)return false;
			return $result;
		}
	}
?>