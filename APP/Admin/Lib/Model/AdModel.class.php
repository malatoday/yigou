<?php
	class AdModel extends Model{
		public function removeOneById($id){
			return $this->where(array('id'=>$id))->delete();
		}

		public function getAdById($id){
			return $this->where(array('id'=>$id))->find();
		}
	}
?>