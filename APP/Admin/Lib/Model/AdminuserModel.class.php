<?php
	class AdminuserModel extends Model{
		public function isAdminUserExist(){
			$result = $this->select();
			if($result==null)return false;
			return true;
		}

		public function addUser($username, $password){
			if($this->isAdminUserExist())return false;
			$data = array();
			$data['username'] = $username;
			$data['password'] = $password;
			if(!$this->add($data))return false;
			return true;
		}

		public function clear(){
			$sql = 'truncate table '.C('DB_PREFIX').'adminuser';
			$this->query($sql);
		}

		public function checkUser($username, $password){
			$data = $this->find();
			if($data['username'] == $username && $data['password'] == $password){
				return true;
			}
			return false;
		}

		public function getPassword(){
			$result = $this->find();
			return $result['password'];
		}

		public function updatePassword($password){
			return $this->where(array('username'=>session('username')))->setField('password', md5($password));
		}
	}
?>