<?php
	class LoanmessageModel extends Model{
		public function addLoan($data){
			if(!$this->add($data))return false;
			return true;
		}

		public function getAllLoan(){
			return $this->order('limit_time desc')->select();
		}

		public function getLoanById($id){
			return $this->where(array('id'=>$id))->find();
		}

		public function removeLoanById($id){
			return $this->where(array('id'=>$id))->delete();
		}

		public function updateLoan($data){
			return $this->save($data);
		}

		public function getLoanCount(){
			$data = $this->select();
			return count($data);
		}
	}
?>