<?php
	class RegionModel extends Model{
		public function getAllPids(){
			return $this->where(array('pid'=>0))->select();
		}

		public function getAllData(){
			return $this->select();
		}

		public function getRegionNameById($id){
			return $this->where(array('id'=>$id))->getField('name');
		}

		public function getRegionPid($id){
			return $this->where(array('id'=>$id))->getField('pid');
		}

		public function removeById($id){
			$data = $this->where(array('pid'=>$id))->select();
			foreach($data as $v){
				$this->removeById($v['id']);
			}
			$this->where(array('id'=>$id))->delete();
		}
	}
?>