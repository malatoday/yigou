<?php
	class CategoryModel extends Model{
		private $Lunbotu = array(2, 3, 4, 5); 
		private $Ad = array(10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21);
		private $SellerRoot = 22;

		public function readAllCategorys(){
			$result = array();
			$result = $this->select();
			return $result;
		}

		public function addACategory($name, $pid){
			$data = array();
			$data['name'] = $name;
			$data['pid'] = $pid;
			return $this->add($data);
		}

		public function getCategoryNameById($id){
			$result = $this->where(array('id'=>$id))->getField('name');
			if($result == null)$result = '/';
			return $result;
		}

		public function getPidById($id){
			return $this->where(array('id'=>$id))->getField('pid');
		}

		public function removeCategoryById($id){
			$result = $this->where(array('pid'=>$id))->select();
			if($result!=null){
				foreach($result as $v){
					$this->removeCategoryById($v['id']);
				}
			}
			$this->where(array('id'=>$id))->delete();
		}

		public function getLunbotuCategory(){
			$result = array();
			foreach($this->Lunbotu as $v){
				$result[] = $this->where(array('id'=>$v))->find();
			}
			return $result;
		}

		public function getAdCategory(){
			$result = array();
			foreach ($this->Ad as $v) {
				$result[] = $this->where(array('id'=>$v))->find();
			}
			return $result;
		}

		public function getSellerCategory(){
			$result = array();
			$result = $this->where(array('pid'=>$this->SellerRoot))->select();
			return $result;
		}
	}
?>