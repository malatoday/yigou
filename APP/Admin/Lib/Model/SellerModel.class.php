<?php
	class SellerModel extends Model{
		public function getSellerById($id){
			return $this->where(array('id'=>$id))->find();
		}

		public function updateSeller($data){
			return $this->save($data);
		}

		public function getAllSeller(){
			return $this->select();
		}
	}
?>