<?php
	class AreaModel extends Model{
		public function addArea($data){
			$res = $this->add($data);
			if(!$res)return false;
			return $res;
		}

		public function updateArea($data){
			$res = $this->save($data);
			if($res===false)return false;
			return true;
		}

		public function getAreaProvinceById($id){
			return $this->where(array('id'=>$id))->getField('province');
		}

		public function getAreaCityById($id){
			return $this->where(array('id'=>$id))->getField('city');
		}

		public function getAreaAreaById($id){
			return $this->where(array('id'=>$id))->getField('area');
		}

		public function getAreaMoreById($id){
			return $this->where(array('id'=>$id))->getField('more');
		}

		public function getAreaById($id){
			return $this->where(array('id'=>$id))->find();
		}
	}
?>