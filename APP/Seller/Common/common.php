<?php
	function array_to_string($arr){
		$res = '';
		foreach($arr as $v){
			$res.=$v;
			$res.='*';
		}
		return $res;
	}

	function isLogin(){
		if(session('app_id')!='YIGOU_SELLER')return false;
		return true;
	}

	function friendlyDateFormat($timestamp){
		if($timestamp==0)return '未定';
		return date('Y-m-d H:i:s', $timestamp);
	}

	function getBeType($typeid){
		switch($typeid){
			case C('BE_STATUS_ORDERING'):
				return '预约中';
			break;
			case C('BE_STATUS_ORDERSUCCESS'):
				return '预约成功';
			break;
			case C('BE_STATUS_ORDERFAILD'):
				return '预约失败';
			break;
			case C('BE_STATUS_ONLINE'):
				return '上线中';
			break;
			case C('BE_STATIS_OFFLINE'):
				return '产品下线';
			break;
			case C('BE_STATIS_CHECKOUT'):
				return '结账中';
			break;
			default:
				return 'ERROR';
			break;
		};
	}
?>