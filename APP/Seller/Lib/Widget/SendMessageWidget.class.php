<?php
	class SendMessageWidget extends Widget{
		public function render($data){
			switch ($data['type']) {
				case C('MSG_RESGITER'):
					$content = $this->renderFile('registermsg', $data);
					break;
				case C('MSG_CHANGEPHONE'):
					$content = $this->renderFile('changephone', $data);
					break;
				case C('MSG_ADDPOINT'):
					$content = $this->renderFile('addpoint', $data);
					break;
				default:
					# code...
					break;
			}
			return $content;
		}
	}
?>