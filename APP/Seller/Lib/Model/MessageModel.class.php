<?php
	class MessageModel extends Model{
		public function send($uid, $data, $msgtype){
			// $con = W('SendMessage', $data, true);
			// 将$data 转换成字符串
			$con = array_to_string($data);
			$tmp = array();
			$tmp['uid'] = $uid;
			$tmp['mtime'] = time();
			$tmp['content'] = $con;
			$tmp['msgtype'] = $msgtype;
			$tmp['hasread'] = 0;
			if(!$this->add($tmp))return false;
			return true;
		}
	}
?>