<?php
// 本类由系统自动生成，仅供测试用途
class IndexAction extends Action {

	public function __construct(){
		header("Content-type:text/html;charset=utf-8");
			if(!isLogin()){
				$this->redirect('Public/index');
			}
			parent::__construct();
		}

    public function index(){
		$this->display();
	}

	public function myinfo(){
		$Seller = M('Seller');
		$info = $Seller->where(array('username'=>session('username')))->find();
		$this->assign('data', $info);
		$this->display();
	}

	public function auth(){
		// 代金券/优惠券认证
		$this->display();
	}

	public function bill(){
		// 账单
		$Seller = M('Seller');
		$sellerid = $Seller->where(array('username'=>session('username')))->getField('id');
		$Bill = M('Bill');
		$data = $Bill->where(array('seller'=>$sellerid))->select();
		$this->assign('data', $data);
		$this->display();
	}
	
	public function startVerify(){
		// 验证
		if(!isset($_POST['phone']))$this->redirect(U('Index/auth'), 0);
		$phone = $this->_post('phone');
		$User = M('User');
		$Seller = M('Seller');
		$Details = M('Details');
		$Voucher = M('Voucher');
		$Coupon = M('Coupon');
		$uid = $User->where(array('username'=>$phone))->getField('id');
		if($uid==null)$this->error('不存在的账号');
		$sellerid = $Seller->where(array('username'=>session('username')))->getField('id');
		$details = $Details->where(array('seller'=>$sellerid))->select();
		$voucher = $Voucher->where(array('uid'=>$uid, 'sid'=>$sellerid, 'used'=>0))->find();
		$coupon = $Coupon->where(array('uid'=>$uid, 'sid'=>$sellerid, 'used'=>0))->find();
		/*
		foreach($details as $v){
			$res1[] = $Voucher->where(array('uid'=>$uid, 'type'=>$v['type'], 'gid'=>$v['lid'], 'used'=>0))->find();
			$res2[] = $Coupon->where(array('uid'=>$uid, 'type'=>$v['type'], 'gid'=>$v['lid'], 'used'=>0))->find();
		}*/
		/*
		$i = 0;
		while($voucher!=null or $i>=count($res1)){
			if($res1[$i]!=null){
				$voucher = $res1[$i];
			}
		}
		$i = 0;
		while($coupon!=null or $i>=count($res2)){
			if($res2[$i]!=null){
				$coupon = $res2[$i];
			}
		}*/
		/*
		foreach($res1 as $v){
			if($v!=null){
				$voucher = $v;
			}
		}
		foreach($res2 as $v){
			if($v!=null){
				$coupon = $v;
			}
		}
		*/
		
		if($voucher==null && $coupon==null)$this->error('顾客尚未拥有本店优惠资格');
		$this->assign('voucher', $voucher);
		$this->assign('coupon', $coupon);
		$this->assign('uid', $uid);
		$this->display();
	}

	public function getVerify(){
		if($this->_post('pre')=='')$this->error('你没有输入客户消费金额');
		if($this->_post('suf')=='NaN' || $this->_post('suf')=='')$this->error('实际消费金额错误');
		$pre = $this->_post('pre');
		$suf = $this->_post('suf');
		$uid = $this->_post('uid');
		$discount = $this->_post('discount');
		if($discount=='')$discount = 10;
		$value = $this->_post('value');
		if($value=='')$value = 0;
		/**
		 * 暂不作验证
		 * if($suf-($pre*($discount/10.0)-$value)>=10 || $suf-($pre*($discount/10.0)-$value)<=-10){
		 *	// 实际消费金额被恶意篡改
		 *	$this->error('消费金额计算错误，请联系易GO网服务人员');
		 * }
		*/
		
		// 记录账单
		$Seller = M('Seller');
		$Bill = M('Bill');
		$data = array();
		$data['pre'] = $pre;
		$data['suf'] = $suf;
		$data['uid'] = $uid;
		$data['time'] = time();
		$data['discount'] = $discount;
		$data['value_yuan'] = $value;
		$data['seller'] = $Seller->where(array('username'=>session('username')))->getField('id');
		if(!$Bill->add($data))$this->error('账单添加失败');
		// 设置代金券/优惠券为已使用
		$couponid = $this->_post('couponid');
		$voucherid = $this->_post('voucherid');
		if($couponid!=''){
			$Coupon = M('Coupon');
			$Coupon->where(array('id'=>$couponid))->setField('used', 1);
		}
		if($voucherid!=''){
			$Voucher = M('Voucher');
			$Voucher->where(array('id'=>$voucherid))->setField('used', 1);
		}
		// 给用户发送站内信
		$User = M('User');
		$Seller = M('Seller');
		$Message = D('Message');
		$message = array();
		$message['area'] = $Seller->where(array('username'=>session('username')))->getField('area');
		$message['discount'] = $discount;
		$message['suf'] = $suf;
		$message['usedpoints'] = $value*100;
		$Message->send($uid, $message, C('MSG_ADDPOINT'));
		// 给用户增加积分
		$Points = M('Points');
		$point = $Points->where(array('user'=>$uid))->getField('total');
		$newtotal = $point+floor($suf/2);
		if($Points->where(array('user'=>$uid))->setField('total', $newtotal)){
			$this->success('成功', U('Index/index'));
		}
		$this->error('失败');
	}

	public function feedback(){
		$this->display();
	}

	public function feedbackHandle(){
		if($this->_post('text')=='')$this->error('留言内容不能为空');
		$data = array();
		$data['content'] = $this->_post('text');
		$Seller = M('Seller');
		$sellerid = $Seller->where(array('username'=>session('username')))->getField('id');
		$data['seller'] = $sellerid;
		$data['mtime'] = time();
		$Feedback = M('Feedback');
		if(!$Feedback->add($data))$this->error('留言失败');
		$this->success('留言成功', U('Index/index'));
	}

	public function bespeak(){
		// 检查当前商家是不是有已经存在的预约
		$Bespeak = M('Bespeak');
		$Seller = M('Seller');
		$sid = $Seller->where(array('username'=>session('username')))->getField('id');
		$map = array();
		$map['sid'] = array('eq', $sid);
		$map['status'] = array('eq', C('BE_STATUS_ORDERSUCCESS'));
		//$map['status'] = array('in', '\''.C('BE_STATUS_ORDERSUCCESS').','.C('BE_STATUS_ONLINE').'\'');
		//$map['_string'] = 'status='.C('BE_STATUS_ORDERSUCCESS').' OR status='.C('BE_STATUS_ONLINE');
		$data = $Bespeak->where($map)->select();
		if(count($data)!=0)$this->error('你目前已经存在预约成功的内容');
		$map['status'] = array('eq', C('BE_STATUS_ONLINE'));
		$data = $Bespeak->where($map)->select();
		if(count($data)!=0)$this->error('你目前已经存在预约上线的内容');
		$map['status'] = array('eq', C('BE_STATIS_CHECKOUT'));
		$data = $Bespeak->where($map)->select();
		if(count($data)!=0)$this->error('你目前已经存在预约结账中的内容');
		$map['status'] = array('eq', C('BE_STATUS_ORDERING'));
		$data = $Bespeak->where($map)->select();
		if(count($data)!=0)$this->error('你目前已经存在预约中的内容');
		$this->display();
	}

	public function bespeakHandle(){
		if($this->_post('con')=='')$this->error('预约内容不可为空');
		$data = array();
		$Seller = M('Seller');
		$sid = $Seller->where(array('username'=>session('username')))->getField('id');
		$data['sid'] = $sid;	// 本信息属于哪个商家
		$data['audience'] = $this->_post('all');	// 全场
		$data['pre'] = $this->_post('pre');	// 区间折扣起始值
		$data['suf'] = $this->_post('suf');	// 区间折扣终止值
		$data['start'] = $this->_post('start');	// n折起
		$data['con'] = $this->_post('con');	// 具体内容
		$data['status'] = C('BE_STATUS_ORDERING');	// 状态
		$data['keeptime'] = $this->_post('keeptime');	// 预约上线时长
		if($this->_post('needjobbtn')==0){
			$data['jobreq'] = $this->_post('jobreq');	// 招聘
			$data['jobneed'] = 0;
		}else{
			$data['jobneed'] = 1;
		}
		$data['submit_time'] = time();
		$data['pass_time'] = 0;
		$data['online_time'] = 0;
		$data['limited'] = $this->_post('limited');
		$Bespeak = M('Bespeak');
		$res = $Bespeak->add($data);
		if($res==false)$this->error('预约失败');
		$this->success('预约成功', U('Index/index'), 0);
	}

	public function bespeakList(){
		$Seller = M('Seller');
		$sid = $Seller->where(array('username'=>session('username')))->getField('id');
		$Bespeak = M('Bespeak');
		$list = $Bespeak->where(array('sid'=>$sid))->select();
		$this->assign('data', $list);
		$this->display();
	}

	public function editBespeak(){
		$id = $this->_get('id');
		$rdm = $this->_get('random');
		if($rdm!=md5(session('rdm')))return false;
		$Bespeak = M('Bespeak');
		$data = $Bespeak->where(array('id'=>$id))->find();
		// 验证时间是否超过24小时
		if($data==false)return false;
		$now = time();
		$oneday = 24*60*60;
		if($data['submit_time']+$oneday<$now){
			// 已经超过了
			$this->error('距离提交已经超过24小时，无法修改');
		}
		if($data['status']!=C('BE_STATUS_ORDERING'))$this->error('你的预约已经被处理，无法修改，请联系管理员解决');
		$this->assign('data',$data);
		$this->display();
	}

	public function editBespeakHandle(){
		if($this->_post('con')=='')$this->error('预约内容不可为空');
		$data = array();
		$id = $this->_post('id');
		$Seller = M('Seller');
		$sid = $Seller->where(array('username'=>session('username')))->getField('id');
		$data['sid'] = $sid;	// 本信息属于哪个商家
		$data['audience'] = $this->_post('all');	// 全场
		$data['pre'] = $this->_post('pre');	// 区间折扣起始值
		$data['suf'] = $this->_post('suf');	// 区间折扣终止值
		$data['start'] = $this->_post('start');	// n折起
		$data['con'] = $this->_post('con');	// 具体内容
		$data['status'] = C('BE_STATUS_ORDERING');	// 状态
		$data['keeptime'] = $this->_post('keeptime');	// 预约上线时长
		$data['submit_time'] = time();
		$data['pass_time'] = 0;
		$data['online_time'] = 0;
		$data['limited'] = $this->_post('limited');
		$data['id'] = $id;
		$Bespeak = M('Bespeak');
		$res = $Bespeak->save($data);
		if($res==false)$this->error('修改失败');
		$this->success('修改成功', U('Index/index'), 0);
	}

	public function cancelBespeak(){
		$id = $this->_param('id');
		$rdm = $this->_param('random');
		if($rdm != md5(session('rdm')))return false;
		$Bespeak = M('Bespeak');
		$data = $Bespeak->where(array('id'=>$id))->find();
		if($data['status']!=C('BE_STATUS_ORDERING'))$this->error('你的预约已经被处理，无法操作，请联系管理员解决');
		$Bespeak->where(array('id'=>$id))->delete();
		$this->success('删除成功', U('Index/index'), 0);
	}

	public function you_must_know(){
		$this->display();
	}

	public function resetPassword(){
		$this->display();
	}

	public function resetPasswordHandle(){
		$old = $this->_post('oldpassword');
		$new = $this->_post('newpassword');
		$repeat = $this->_post('repeatpassword');
		if($old=='' || $new=='' || $repeat==''){
			$this->error('输入的数据不合法');
		}
		if($repeat!=$new){
			$this->error('两次输入的密码不一致');
		}
		$Seller = M('Seller');
		$info = $Seller->where(array('id'=>session('sid')))->find();
		if($info['password']!=md5($old)){
			$this->error('密码错误');
		}
		if($Seller->where(array('id'=>session('sid')))->setField('password', md5($new))===false){
			$this->error('密码更新失败');
		}
		$this->success('密码更新成功', U('Index/index'));
	}
}