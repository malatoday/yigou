<?php
	class PublicAction extends Action{
		public function index(){
			$this->display();
		}

		public function checkLogin(){
			$Seller = M('Seller');
			$username = $this->_post('username');
			$password = $this->_post('password');
			$res = $Seller->where(array('username'=>$username))->find();
			if($res==null)$this->error('用户名错误');
			if($res['password']!=md5($password))$this->error('密码错误');
			session('app_id', 'YIGOU_SELLER');
			session('username', $res['username']);
			session('company_name', $res['company_name']);
			session('name', $res['name']);
			session('tel', $res['tel']);
			session('sid', $res['id']);
			session('rdm', $this->random(6,1));
			$this->success('登陆成功', U('Index/index'));
		}

		public function logout(){
			session(null);
			$this->success('退出成功', U('Index/index'));
		}

		public function register(){
			$this->display();
		}

		public function registerHandle(){
			dump($_POST);
		}

		private function random($length = 6 , $numeric = 0) {
		PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
		if($numeric) {
			$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
		} else {
			$hash = '';
			$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
			$max = strlen($chars) - 1;
			for($i = 0; $i < $length; $i++) {
				$hash .= $chars[mt_rand(0, $max)];
			}
		}
		return $hash;
	}
	}
?>