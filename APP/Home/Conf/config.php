<?php
return array(
	'DB_TYPE'   => 'mysql', // 数据库类型
    'DB_HOST'   => 'localhost', // 服务器地址
    'DB_NAME'   => 'yigou', // 数据库名
    'DB_USER'   => 'root', // 用户名
    'DB_PWD'    => '', // 密码
    'DB_PORT'   => 3306, // 端口
    'DB_PREFIX' => 'yigou_', // 数据库表前缀 	
    'Category_index'	=>	2,	// 主导航首页分类
    'Category_shanGoLife'	=>	3,	// 闪GO会
    'Category_superShanGo'	=>	4,	// 超级闪GO
    'Category_qinsongYidai'	=>	5,	// 易贷会
    'COOKIE_PREFIX' =>  'yigou_',
    'Spread_rate'   =>  50,
    'Type_student'  =>  1,
    'Type_company'  =>  2,
    'Type_collar'   =>  3,
    'Type_other'    =>  4,
    'Lifeserve' =>  1,
    'Loanmessage'   =>  2,
    'MIN_POINTS'    =>  100,
    'POINTS_TO_YUAN'    =>  100,
    'MSG_RESGITER'  =>  1,
    'MSG_CHANGEPHONE'   =>  2,
    'MSG_ADDPOINT'  =>  3,
    'DX_sn'    => 'SUD-KEY-010-00249', // 短信验证码帐号
    'DX_pwd'  =>  '289AFC4853596E1D57D69CC49A331E04',  // 短信验证码密文
    'DX_md5sign'   =>  '4r9t3k', // 短信验证码md5密钥
    'TMPL_ACTION_ERROR' => 'Public:error',
    'TMPL_ACTION_SUCCESS' => 'Public:success',
    'SHOW_PAGE_TRACE' =>false
);
?>