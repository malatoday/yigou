<?php
	function isLogin(){
		if(session('app_id')!='YIGOU_HOME')return false;
		return true;
	}

	function getLunbotuCount(){
		$Lunbotu = D('Lunbotu');
		$count = $Lunbotu->getAvailableCount();
		return $count;
	}

	function getPicArray(){
		$Lunbotu = D('Lunbotu');
		$result = $Lunbotu->getAvailableArray();
		return $result;
	}

	function getPicData($id){
		$Pic = D('Pic');
		return $Pic->where(array('id'=>$id))->find();
	}

	function getIndexGoods(){
		$Loan = D('Loanmessage');
		return $Loan->getAllLoansOfIndex();
	}

	function getAreaProvinceById($id){
		$Area = D('Area');
		return $Area->getAreaProvinceById($id);
	}

	function getAreaCityById($id){
		$Area = D('Area');
		return $Area->getAreaCityById($id);
	}

	function getAreaAreaById($id){
		$Area = D('Area');
		return $Area->getAreaAreaById($id);
	}
	
	function getAreaMoreById($id){
		$Area = D('Area');
		return $Area->getAreaMoreById($id);
	}

	function friendlyDateFormat($timestamp){
		return date('Y-m-d h:i:s', $timestamp);
	}

	function shortDateFormat($timestamp){
		return date('Y-m-d', $timestamp);
	}

	function getRegionList(){
		if(empty(session('region_name'))){
			if(empty(cookie('region_name'))){
				$region_name = '重庆';
			}else{
				$region_name = cookie('region_name');
			}
		}else{
			$region_name = session('region_name');
		}
		$Region = D('Region');
		$region_id = $Region->getIdByname($region_name);
		$result = $Region->getAllByPid($region_id);
		return $result;
	}

	function getGoodArea($type, $id){
		$Life = D('Lifeserve');
		$Loan = D('Loanmessage');
		if($type==C('Lifeserve')){
			$areaid = $Life->where(array('id'=>$id))->getField('area');
		}else{
			$areaid = $Loan->where(array('id'=>$id))->getField('area');
		}
		
		$province = getAreaProvinceById($areaid);
		$city = getAreaCityById($areaid);
		$area = getAreaAreaById($areaid);
		$more = getAreaMoreById($areaid);
		$res = $province.$city.$area.$more;
		return $res;
	}

	function array_to_string($arr){
		$res = '';
		foreach($arr as $v){
			$res.=$v;
			$res.='*';
		}
		return $res;
	}

	function string_to_array($str){
		$res = explode('*', $str);
		return $res;
	}

	function getName(){
		$User = D('User');
		$nick = $User->where(array('userid'=>session('userid')))->getField('nick');
		return $nick;
	}

	function getDataByTypeId($type, $id){
		$Lifeserve = D('Lifeserve');
		$Loanmessage = D('Loanmessage');
		if($type==C('Lifeserve')){
			$res = $Lifeserve->where(array('id'=>$id))->find();
		}else{
			$res = $Loanmessage->where(array('id'=>$id))->find();
		}
		return $res;
	}

	/**
	 * @param $uid 用户id
	 * @return bool 操作成功与否
	 * 检查代金券是否过期，如果过期且未使用，删除代金券并且返还积分
	*/
	function checkVoucher($uid){
		$Voucher = M('Voucher');
		$Points = D('Points');
		$data = $Voucher->where(array('uid'=>$uid, 'used'=>0))->select();
		foreach($data as $v){
			if((int)$v['limit_time']<=time()){
				$Voucher->where(array('id'=>$v['id']))->delete();
				$Points->returnPoints((int)$v['value_yuan']*C('POINTS_TO_YUAN'), $uid);
			}
		}
		return true;
	}

	function getDiscountByLid($lid){
		$Lifeserve = M('Lifeserve');
		$res = $Lifeserve->where(array('id'=>$lid))->find();
		return  $res['discount'];
	}
    
    // 获取商品信息的商家名称
    function getNameOfSeller($type, $gid){
        $Lifeserve = M('Lifeserve');
        $Loanmessage = M('Loanmessage');
        if($type==1){
            $table = $Lifeserve;
        }else{
            $table = $Loanmessage;
        }
        $data = $table->where(array('id'=>$gid))->find();
        $name = $data['company_name'];
        return $name;
    }
?>