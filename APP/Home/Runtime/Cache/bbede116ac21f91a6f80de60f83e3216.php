<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
        <title>易GO--轻松出发，一站到位</title>
        <link rel="shortcut icon" type="image/x-icon" href="__PUBLIC__/img/favicon.ico" />
        <!-- Bootstrap -->
        <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
        <link href="__PUBLIC__/css/core.css" rel="stylesheet">
        <script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
    </head>
    <body>
		<div class="header">
    <nav class="navbar navbar-default main_nav">
        <div class="container">
			<div class="title-left">
				<span>易GO带您出发！</span>
			</div>
            <div  class="title-right right">
            	<?php if(isLogin()){ ?>
            		<span>欢迎你！<?php echo session('nick'); ?></span>
            		<span><a href="<?php echo U('Public/logout');?>" class="link">退出</a></span>
            	<?php }else{ ?>
					<span><a href="<?php echo U('Public/login');?>" class="link">登陆</a>|<a href="<?php echo U('Public/register');?>" class="link">注册</a></span>
				<?php } ?>

				<span><a href="<?php echo U('Myeg/index');?>" class="link">个人中心</a></span>
				<span><a href="<?php echo U('Myeg/mycollect');?>" class="link">我的收藏</a></span>
				<!--
					<span><a href="" class="link">客户中心</a></span>
				-->
				<span><a href="http://egword.com/yigou/seller.php" class="link">商户中心</a></span>
				<span><a href="<?php echo U('Static/help');?>" class="link">帮助中心</a></span>
			</div>
        </div>
    </nav>
	<div class="logo text-center row">
	<div class="container">
		<div class="col-xs-3">
			<img src="__PUBLIC__/img/bannerl.jpg" class="img-responsive pull-right" style="height:100px;margin-top:2em;">
		</div>
		<div class="col-xs-6">
			<img src="__PUBLIC__/img/logo.png" class="img-responsive logo-img">
		</div>
		</div>
	</div>
	<div class="jumbotron banner">
		<div class="container ">
			<!--
<h1 class="text-center"><span class="yi">易</span><span class="go">&nbsp;GO</span></h1>
			-->
			<div class="text-center mbanner">
				<a href="<?php echo U('Index/index');?>"><img class="img-responsive " src="__PUBLIC__/img/banner.png"></a>
			</div>
			<div class="row header-form">
				<div class="col-xs-3">
					<div class="input-group">
						<label class="input-group-addon "><span><img src="__PUBLIC__/img/location.png" class="img-responsive location"></span><span class="iamhere">我在这儿</span></label>
						<div class="input-group-btn">
							<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<?php if(empty(session('region_name'))){ if(empty(cookie('region_name'))){ echo "重庆"; }else{ echo cookie('region_name'); } }else{ echo session('region_name'); } ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="caret"></span></button>
							<ul class="dropdown-menu dropdown-menu-right" role="menu">
								<?php echo W('ShowRegion');?>
							</ul>
						</div>
					</div>
				</div>
				<div class="col-xs-1">&nbsp;</div>
				<div class="col-xs-4 text-right">
					<form action="<?php echo U('Index/search');?>" method="get">
						<div class="input-group">
							<input name="key" type="text" class="form-control mysearch" placeholder="&nbsp;&nbsp;搜索">
							<span class="input-group-btn">
								<button class="btn btn-default mysearch mysearchbtn" type="submit">&nbsp;&nbsp;<span class="glyphicon glyphicon-search"> </span>&nbsp;&nbsp;</button>
							</span>
						</div>
					</form>
					
				</div>
			</div>
			<div class="col-xs-11 icons text-right">
			<ul class="yigou-icons-list">
				<li><span><img src="__PUBLIC__/img/icon1.png" class="img-responsive micons"> </span><label>&nbsp;100%放心&nbsp;&nbsp;&nbsp;&nbsp;</label></li>
				<li><span><img src="__PUBLIC__/img/icon2.png" class="img-responsive micons"> </span><label>&nbsp;闪电更新&nbsp;&nbsp;&nbsp;&nbsp;</label></li>
				<li><span><img src="__PUBLIC__/img/icon3.png" class="img-responsive micons"> </span><label>&nbsp;无上限优惠&nbsp;&nbsp;&nbsp;&nbsp;</label></li>
				<li><span><img src="__PUBLIC__/img/icon4.png" class="img-responsive micons"> </span><label>&nbsp;万人信赖&nbsp;&nbsp;&nbsp;&nbsp;</label></li>
			</ul>
				
			</div>
		</div>
	</div>
	<div class="index-nav ">
		<div class="container">
			<ul class="nav nav-pills nav-justified">
				<li class="active" role="presentation"><a href="<?php echo U('Index/index');?>" class="<?php echo ($index); ?>">首页</a></li>
				<li class="active" role="presentation"><a href="<?php echo U('Index/blinkEG');?>" class="<?php echo ($blinkEG); ?>">闪GO会</a></li>
				<li class="active" role="presentation"><a href="<?php echo U('Index/superEG');?>" class="<?php echo ($superEG); ?>">超级闪GO</a></li>
				<!--
<li class="active" role="presentation"><a href="<?php echo U('Index/edh');?>" class="<?php echo ($edh); ?>">易贷会</a></li>
				-->
				
			</ul>
			</div>
		</div>
	</div>
</div>
        <?php echo ($lunbotu); ?>
		<div class="main-content">

		
<div class="container">
	<div class="col-xs-8 left mycontent">
		<div class="mybreadcrumb">
			<?php
 $region_list = getRegionList(); ?>
			城市选择：<a href="<?php echo U('Index/index');?>"><span class="label <?php if(!isset($region))echo 'label-selected'; ?>">所有</span></a>
			<?php if(is_array($region_list)): foreach($region_list as $key=>$v): ?><a href="<?php echo U('Index/index', array('region'=>$v['id']));?>"><span class="label <?php if($region==$v['id'])echo 'label-selected'; ?>"><?php echo ($v["name"]); ?></span></a><?php endforeach; endif; ?>
		</div>
		
		
		<?php if(is_array($data)): foreach($data as $key=>$v): if(isset($v['discount'])){ ?>
			<?php echo W('ShowGoods', array('id'=>$v['id'], 'type'=>C('Lifeserve')));?>
			<?php }else{ ?>
			<?php echo W('ShowGoods', array('id'=>$v['id'], 'type'=>C('Loanmessage')));?>
			<?php } endforeach; endif; ?>
	</div>
	<div class="col-xs-4 right">
		<?php  ?>
		<div class="col-xs-12 ad ">
			<!--本站广告-->
			<?php echo W('ShowAd', array('type'=>10));?>
			<!--轮播图广告-->
			<?php echo W('ShowAd', array('type'=>14));?>
			<!--无限广告-->
			<?php echo W('ShowAd', array('type'=>18));?>
		</div>
	</div>
</div>
		</div>
		<div class="footer">
		<div class="row icons">
			<div class="container">
			<div class="col-xs-2 "><img src="__PUBLIC__/img/1.png" class="img-responsive"><div class="text-center">100%放心</div></div>
			<div class="col-xs-2 "><img src="__PUBLIC__/img/2.png" class="img-responsive"><div class="text-center">闪电更新</div></div>
			<div class="col-xs-2 "><img src="__PUBLIC__/img/3.png" class="img-responsive"><div class="text-center">无上限优惠</div></div>
			<div class="col-xs-2 "><img src="__PUBLIC__/img/4.png" class="img-responsive"><div class="text-center">万人信赖</div></div>
			<div class="col-xs-2 "><img src="__PUBLIC__/img/5.png" class="img-responsive"><div class="text-center">金牌商家</div></div>
			<div class="col-xs-2 "><img src="__PUBLIC__/img/6.png" class="img-responsive"><div class="text-center">VIP服务</div></div>
		</div>
		</div>
	<div class="row footer-menu">
		<div class="container">
			<div class="col-xs-1 text-center"> </div>
			<div class="col-xs-2 text-center"><a href="<?php echo U('Static/aboutUs');?>">关于我们</a></div>
			<div class="col-xs-2 text-center"><a href="<?php echo U('Static/brandInvestment');?>">品牌招商</a></div>
			<div class="col-xs-2 text-center"><a href="<?php echo U('Static/privacy');?>">隐私条款</a></div>
			<div class="col-xs-2 text-center"><a href="<?php echo U('Static/userExperienceProject');?>">用户体验计划</a></div>
			<div class="col-xs-2 text-center"><a href="<?php echo U('Static/connectUs');?>">联系我们</a></div>
			<div class="col-xs-1"> </div>
		</div>
		</div>
</div>
<nav class="navbar navbar-default">
		<div class="text-center ">

		<div class="panel">
			<div class="panel-body container">
				版权所有 &copy;重庆易狗网网络科技有限公司
			</div>
		</div>
	</div>
</nav>
        
        <script type="text/javascript" src="__PUBLIC__/js/timer.js"></script>
        <script src="__PUBLIC__/js/bootstrap.min.js"></script>
    </body>
</html>