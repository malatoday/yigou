<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>易GO--轻松出发，一站到位</title>
        <link rel="shortcut icon" type="image/x-icon" href="__PUBLIC__/img/favicon.ico" />
        <!-- Bootstrap -->
        <script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
        <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
        <link href="__PUBLIC__/css/core.css" rel="stylesheet">
    </head>
    <body class="register">
		<div class="header">
			<div class="logo text-center row">
		<div class="col-xs-3">
			<img src="__PUBLIC__/img/bannerl.jpg" class="img-responsive pull-right" style="height:100px;margin-top:2em;">
		</div>
		<div class="col-xs-6">
			<img src="__PUBLIC__/img/logo.png" class="img-responsive logo-img">
		</div>
		
	</div>
			<div class="jumbotron banner">
				<div class="container">
					<div class="text-center mbanner">
						<a href="<?php echo U('Index/index');?>">
							<img class="img-responsive " src="__PUBLIC__/img/banner.png">
						</a>
					</div>
				</div>				
			</div>
			<div class="container icon">
				<div class="col-xs-12 icons text-center">
					<span><img src="__PUBLIC__/img/icon5.png" class="img-responsive micons"> </span><label>&nbsp;100%放心&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon6.jpg" class="img-responsive micons"> </span><label>&nbsp;闪电更新&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon7.jpg" class="img-responsive micons"> </span><label>&nbsp;无上限优惠&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon8.jpg" class="img-responsive micons"> </span><label>&nbsp;万人信赖&nbsp;&nbsp;&nbsp;&nbsp;</label>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="container">
				<div class="col-xs-7 login-logo">
					<img src="__PUBLIC__/img/login.png" class="img-responsive">
				</div>
				<div class="col-xs-5">
					<div class="col-xs-9 register-form">
						<form action="<?php echo U('Public/registerHandle');?>" method="post">
							<div class="col-xs-12 title">
								<label>不是会员？</label>
								<label class="pull-right">已有账号？请<a href="<?php echo U('Public/login');?>">登录</a></label>
								<label class="pull-right"> 请注册 | </label>
							</div>
							
							<div class="form-group">
								<input class="form-control" type="text" name="username" id="mobile" placeholder="手机号"><span id="tusername" class="label label-danger  form-control">手机号不可为空</span>
							</div>
							<div class="form-group">
								<div class="form-inline">
									<input class="form-control" name="phonecode" type="text" placeholder="验证码">
									<a class="btn btn-default pull-right" href="#" id="getVerifyCodeBtn" role="button">获取验证码</a>
								</div>
							</div>
							<div class="form-inline">
								<div class="radio">
									<label>
										<input type="radio" name="sex" id="optionsRadios1" value="男" checked>
										先生
									</label>
								</div>
								<div class="radio">
									<label>
										<input type="radio" name="sex" id="optionsRadios1" value="女" checked>
										女士
									</label>
								</div>
							</div>
							<div class="form-group">
								<input class="form-control" type="password" id="password" name="password" placeholder="密码">
								<span id="tpassword" class="label label-danger  form-control">密码不可为空</span>
							</div>
							<div class="form-group">
								<input class="form-control" type="password" id="repeat" name="repeat" placeholder="确认密码">
								<span id="trepeat" class="label label-danger  form-control">重复密码不可为空</span>
							</div>
							<div class="form-group">
								<div class="form-inline">
									<div class="input-group">
										<input class="form-control" type="text" name="code" placeholder="验证码">
										<div class="input-group-addon verify" id="randomverify">
											<img  src="<?php echo U('Public/verify');?>" class="img-responsive">
										</div>

									</div>
									<span><a id="changerandomverify" >换一张</a></span>
									
								</div>
							</div>
							<div class="form-group">
								<input id="invitecode" class="form-control" type="text" name="invitecode" placeholder="推广码">
								<span id="tinvitecode" class="label label-danger  form-control">推广码无效</span>
							</div>
							<div class="form-group">
									<input class="form-control pinkbtn" id="submit" type="submit" value="注册">
							</div>
							<div class="checkbox">
								<div class="col-xs-12">
									<label id="lawBtn">
									  <input id="law" type="checkbox" checked="checked">我已阅读并接受<a>易GO服务条款</a>
									</label>
								</div>
							</div>
						</form>
						<script type="text/javascript">
						$(window).ready(function(){
							document.getElementById('tpassword').style.display="none";
							document.getElementById('tusername').style.display="none";
							document.getElementById('trepeat').style.display="none";
							document.getElementById('tinvitecode').style.display="none";
							$('#tinvitecode').attr('display', 'none');
							$('#invitecode').attr('ban', 'false');
							$('#submit').click(function(){
								if($('#mobile').val()==''){
									document.getElementById('tusername').style.display="";
									return false;
								}
								if($('#password').val()==''){
									document.getElementById('tpassword').style.display="";
									return false;
								}
								if($('#repeat').val()==''){
									document.getElementById('tpassword').style.display="";
									return false;
								}
								if($('#invitecode').attr('ban')=='true')return false;
								return true;
							});
							$('#mobile').keyup(function(){
								if($('#mobile').val()==''){
									document.getElementById('tusername').style.display="";
								}else{
									document.getElementById('tusername').style.display="none";
									// Check mobile
									var reg = /^\d{11}$/;
									var str = $('#mobile').val();
									if(reg.test(str)){
										$.post('http://egword.com/yigou/index.php/Public/checkHasRes', {mobile:str, 'dtype': 'jsonp'}, function(data, status){
											if(data.status==0){
												// set btn can not use
												$('#getVerifyCodeBtn').addClass('disabled');
												console.log(data);
											}else{
												// set btn can use
												$('#getVerifyCodeBtn').removeClass('disabled');
											}
										});
									}else{
										// set btn can not use
										$('#getVerifyCodeBtn').addClass('disabled');
									}
								}
							});
							$('#mobile').change(function(){
								clearInterval();
								if($('#mobile').val()==''){
									document.getElementById('tusername').style.display="";
								}else{
									document.getElementById('tusername').style.display="none";
									// Check mobile
									var reg = /^\d{11}$/;
									var str = $('#mobile').val();
									if(reg.test(str)){
										$.post('http://<?php echo $_SERVER['HTTP_HOST'];?>/yigou/index.php/Public/checkHasRes', {mobile:str, 'dtype': 'jsonp'}, function(data, status){
											if(data.status==0){
												// set btn can not use
												$('#getVerifyCodeBtn').addClass('disabled');
											}else{
												// set btn can use
												$('#getVerifyCodeBtn').removeClass('disabled');
											}
										});
									}else{
										// set btn can not use
										$('#getVerifyCodeBtn').addClass('disabled');
									}
								}
							});
							$('#mobile').blur(function(){
								if($('#mobile').val()==''){
									document.getElementById('tusername').style.display="";
								}else{
									document.getElementById('tusername').style.display="none";

								}
							});

							$('#password').change(function(){
								if($('#password').val()==''){
									document.getElementById('tpassword').style.display="";
								}else{
									document.getElementById('tpassword').style.display="none";
								}
							});
							$('#password').blur(function(){
								if($('#password').val()==''){
									document.getElementById('tpassword').style.display="";
								}else{
									document.getElementById('tpassword').style.display="none";
								}
							});
							$('#repeat').change(function(){
								if($('#repeat').val()==''){
									document.getElementById('trepeat').style.display="";
								}else{
									document.getElementById('trepeat').style.display="none";
								}
							});
							$('#repeat').blur(function(){
								if($('#repeat').val()==''){
									document.getElementById('trepeat').style.display="";
								}else{
									document.getElementById('trepeat').style.display="none";
								}
							});
							$('#invitecode').keyup(function(){
								var code = $('#invitecode').val();
								if(code==''){
									// 不显示报错信息
									document.getElementById('tinvitecode').style.display="none";
									$('#invitecode').attr('ban', 'false');
									return;
								}
								if(/^[0-9]{6}$/.test(code)){
									$.post('http://<?php echo $_SERVER['HTTP_HOST'];?>/yigou/index.php/Public/checkInvitecode', {invitecode:code, 'dtype':'jsonp'}, function(data, status){
										if(data.status==1){
											// 检测正确
											document.getElementById('tinvitecode').style.display="none";
											$('#invitecode').attr('ban', 'false');
										}else{
											// 检测错误
											document.getElementById('tinvitecode').style.display="";
											$('#tinvitecode').attr('display', 'block');
											$('#invitecode').attr('ban', 'true');
										}
									});
									return;
								}
								$('#invitecode').attr('ban', 'true');
								document.getElementById('tinvitecode').style.display="";
							});
						});
						
						</script>
					</div>
					
				</div>
			</div>
		</div>
		<div class="footer">
			<nav class="navbar navbar-default">
				<div class="panel">
					<div class="panel-body text-center">
						版权所有 &copy;重庆易狗网网络科技有限公司
					</div>
				</div>
			</nav>
		</div>

		<script type="text/javascript">
		window.onload=function(){
			$('#getVerifyCodeBtn').click(function(){
				function checkMobile(str){
					var reg = /^\d{11}$/;
					if(reg.test(str))return true;
					return false;
				}
				var mobile = $('#mobile').val();
				if(checkMobile(mobile)==false){
					return;
				}else{
					$('#getVerifyCodeBtn').addClass('disabled');
					$('#getVerifyCodeBtn').text('输入验证码');
					$.post('http://<?php echo $_SERVER['HTTP_HOST'];?>/yigou/index.php/Index/setSession', {send_code:'yigou', 'dtype': 'jsonp'}, function(data, status){
						console.log(data);
						$.post('http://<?php echo $_SERVER['HTTP_HOST'];?>/yigou/index.php/Index/sendVerifyCode', {mobile:$('#mobile').val(), send_code:'yigou', 'dtype': 'jsonp'}, function(data, status){
							console.log(data);
							var total = 60;
							$('#mobile').attr('readonly', 'true');
							var timers = setInterval(function(){
								$('#getVerifyCodeBtn').text(''+total+'秒后重试');
								total--;
								if(total<=0){
									$('#getVerifyCodeBtn').removeClass('disabled');
									$('#getVerifyCodeBtn').text('获取验证码');
									console.log(total);
									clearInterval(timers);
									$('#mobile').removeAttr('readonly');
								}
							}, 1000);
						});
					});
					
				}
			});
			$('#submit').click(function(){
				if($('#law').attr('checked')=='')return false;
				$('#mobile').removeAttr('disabled');
				return true;
			});
			$('#lawBtn').click(function(){
				if($('#law').attr('checked')=='')
					$('#law').attr('checked', 'checked');
				$('#law').attr('checked', '');
			});
			$('#changerandomverify').click(function(){
				$('#randomverify').html('<img  src="<?php echo U('Public/verify');?>" class="img-responsive">');
			});
		}
		</script>
	</body>
</html>