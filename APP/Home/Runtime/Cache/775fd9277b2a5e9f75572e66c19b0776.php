<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>易GO--轻松出发，一站到位</title>
        <link rel="shortcut icon" type="image/x-icon" href="__PUBLIC__/img/favicon.ico" />
        <!-- Bootstrap -->

        <script src="__PUBLIC__/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="__PUBLIC__/js/jquery.cookie-1.4.1.min.js"></script>
        <link href="__PUBLIC__/css/bootstrap.min.css" rel="stylesheet">
        <link href="__PUBLIC__/css/core.css" rel="stylesheet">
    </head>
    <body class="login">
		<div class="header">
			<div class="logo text-center row">
		<div class="col-xs-3">
			<img src="__PUBLIC__/img/bannerl.jpg" class="img-responsive pull-right" style="height:100px;margin-top:2em;">
		</div>
		<div class="col-xs-6">
			<img src="__PUBLIC__/img/logo.png" class="img-responsive logo-img">
		</div>
		
	</div>
			<div class="jumbotron banner">
				<div class="container">
					<div class="text-center mbanner">
						<a href="<?php echo U('Index/index');?>">
							<img class="img-responsive " src="__PUBLIC__/img/banner.png">
						</a>
					</div>
				</div>				
			</div>
			<div class="container icon">
				<div class="col-xs-12 icons text-center">
					<span><img src="__PUBLIC__/img/icon5.png" class="img-responsive micons"> </span><label>&nbsp;100%放心&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon6.jpg" class="img-responsive micons"> </span><label>&nbsp;闪电更新&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon7.jpg" class="img-responsive micons"> </span><label>&nbsp;无上限优惠&nbsp;&nbsp;&nbsp;&nbsp;</label>
					<span><img src="__PUBLIC__/img/icon8.jpg" class="img-responsive micons"> </span><label>&nbsp;万人信赖&nbsp;&nbsp;&nbsp;&nbsp;</label>
				</div>
			</div>
		</div>
		<div class="content">
			<div class="container">
				<div class="col-xs-7 login-logo">
					<img src="__PUBLIC__/img/login.png" class="img-responsive">
				</div>
				<div class="col-xs-5">
					<div class="col-xs-9 login-form">
						<form action="<?php echo U('Public/checkLogin');?>" method="post">
							<label>如有账号，请登录</label>
							<div class="form-group">
								<input class="form-control" type="text" id="username" name="username" placeholder="手机号" ><span id="tusername" class="label label-danger  form-control">用户名不可为空</span>
							</div>
							<div class="form-group">
								<input class="form-control" type="password" id="password" name="password" placeholder="密码">
								<span id="tpassword" class="label label-danger  form-control">密码不可为空</span>
							</div>
							<div class="form-group">
								<input class="form-control pinkbtn" id="submit" type="submit" value="登录">
							</div>
							<div class="checkbox">
								<div class="col-xs-5">
									<label>
									  <input value="1" id="check" type="checkbox" name="check">记住用户名
									</label>
								</div>
								<div class="col-xs-7 text-right">
									<span><a href="<?php echo U('Public/findPasswd');?>">忘记密码？</a></span>
									<span><a href="<?php echo U('Public/register');?>">免费注册</a></span>
								</div>
								
							</div>
						</form>
					</div>
					<script type="text/javascript">
					$(window).ready(function(){
						document.getElementById('tpassword').style.display="none";
						document.getElementById('tusername').style.display="none";
						var COOKIE_NAME = 'username';
						if(COOKIE_NAME){
							$('#username').val($.cookie(COOKIE_NAME));
						}
						$("#check").click(function(){
          					if(this.checked){
        						$.cookie(COOKIE_NAME,$("#username").val(), { path: '/', expires: 10 });       
   							}
   						});
						$('#submit').click(function(){
							if($('#username').val()==''){
								document.getElementById('tusername').style.display="";
								return false;
							}
							if($('#password').val()==''){
								document.getElementById('tpassword').style.display="";
								return false;
							}
							if($('#check').checked){
								$.cookie(COOKIE_NAME,$("#username").val(), { path: '/', expires: 10 }); 
							}
							return true;
						});
						$('#username').change(function(){
							if($('#username').val()==''){
								document.getElementById('tusername').style.display="";
							}else{
								document.getElementById('tusername').style.display="none";
							}
						});
						$('#username').blur(function(){
							if($('#username').val()==''){
								document.getElementById('tusername').style.display="";
							}else{
								document.getElementById('tusername').style.display="none";
							}
						});
						$('#password').change(function(){
							if($('#password').val()==''){
								document.getElementById('tpassword').style.display="";
							}else{
								document.getElementById('tpassword').style.display="none";
							}
						});
						$('#password').blur(function(){
							if($('#password').val()==''){
								document.getElementById('tpassword').style.display="";
							}else{
								document.getElementById('tpassword').style.display="none";
							}
						});
					});
					
					</script>
				</div>
			</div>
		</div>
		<div class="footer">
			<nav class="navbar navbar-default">
			<div class="text-center">
				<div class="panel">
					<div class="panel-body">
						版权所有 &copy;重庆易狗网网络科技有限公司
					</div>
				</div>
			</div>
		</nav>
		</div>
	</body>
</html>