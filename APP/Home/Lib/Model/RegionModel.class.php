<?php
	class RegionModel extends Model{
		public function getAllPids(){
			return $this->where(array('pid'=>0))->select();
		}

		public function getPidById($id){
			return $this->where(array('id'=>$id))->getField('pid');
		}

		public function getIdByname($name){
			return $this->where(array('name'=>$name))->getField('id');
		}

		public function getAllByPid($pid){
			return $this->where(array('pid'=>$pid))->select();
		}
	}
?>