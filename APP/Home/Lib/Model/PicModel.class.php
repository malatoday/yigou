<?php
	class PicModel extends Model{
		public function getInfoById($id){
			return $this->where(array('id'=>$id))->find();
		}
		
		/**
		 * @param
		 * @return 图片信息
		 * 获得默认的头像图片
		*/
		public function getDefaultPhoto(){
			$data = array();
			$data['savepath'] = 'pic/';
			$data['savename'] = 'defaultPhoto.jpg';
			$data['name'] = '默认头像';
			return $data;
		}
	}
?>