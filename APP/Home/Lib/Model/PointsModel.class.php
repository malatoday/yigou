<?php
	class PointsModel extends Model{
		public function getNowPointsByUserId($id){
			$data = $this->where(array('user'=>$id))->find();
			if($data == null)return false;
			return $data['total']-$data['used'];
		}

		public function newPoints($point, $id){
			$data = array();
			$data['total'] = $point;
			$data['user'] = $id;
			$data['used'] = 0;
			if(!$this->add($data))return false;
			return true;
		}

		public function addPoints($point, $id){
			// 增加积分
			if(!$this->where(array('user'=>$id))->find())return $this->newPoints($point, $id);
			$res = $this->where(array('user'=>$id))->find();
			$data = array();
			$data['total'] = $res['total'] + $point;
			$data['user'] = $id;
			$data['used'] = $res['used'];
			$data['id'] = $res['id'];
			if(!$this->save($data))return false;
			return true;
		}

		public function cutPoints($point, $id){
			// 消费积分
			$res = $this->where(array('user'=>$id))->find();
			$used = (int)$res['used'] + $point;
			if($this->where(array('user'=>$id))->setField('used', $used)===false)return false;
			return true;
		}

		public function returnPoints($point, $id){
			// 返还积分
			$res = $this->where(array('user'=>$id))->find();
			$used = (int)$res['used'] - $point;
			if($this->where(array('user'=>$id))->setField('used', $used)===false)return false;
			return true;
		}
	}
?>