<?php
	class LoaninfoModel extends Model{
		public function getInfoByUserid($userid){
			return $this->where(array('userid'=>$userid))->find();
		}

		public function getData($category){
			$data = $this->where(array('category'=>$category))->order('myorder desc')->select();
			$Region = D('Region');
			$region_id = session('region_id')==null?3:session('region_id');
			$result = array(); 
			foreach($data as $v){
				if($v['region']==$region_id || $Region->getPidById($v['region'])==$region_id){
					$result[] = $v;
				}
			}
			$res = array();
			// 过滤时间
			foreach($result as $v){
				if($v['limit_time']>time()){
					if($v['myorder']!=0){
						$res[] = $v;
					}
				}
			}
			return $res;
		}
	}
?>