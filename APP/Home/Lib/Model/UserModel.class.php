<?php
	class UserModel extends Model{
		public function checkUser($map){
			if(!$map)return false;
			$data = $this->where($map)->find();
			return $data;
		}

		public function addUser($username, $password){
			$data = array();
			$data['username'] = $username;
			$data['password'] = md5($password);
			$id = $this->add($data);
			if($id){
				if($this->addUserid($id))return true;
			}
			return false;
		}

		public function addUserid($id){
			$num = $this->random(6,1);
			if($this->where(array('userid'=>$num))->find()){
				$this->addUserid($id);
			}else{
				$this->where(array('id'=>$id))->setField('userid', $num);
			}
			return true;

		}

		private function random($length = 6 , $numeric = 0) {
			PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
			if($numeric) {
				$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
			} else {
				$hash = '';
				$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ123456789abcdefghjkmnpqrstuvwxyz';
				$max = strlen($chars) - 1;
				for($i = 0; $i < $length; $i++) {
					$hash .= $chars[mt_rand(0, $max)];
				}
			}
			return $hash;
		}

		public function getUserIdByUserid($userid){
			return $this->where(array('userid'=>$userid))->getField('id');
		}

		public function getIdByUsername($username){
			return $this->where(array('username'=>$username))->getField('id');
		}

		public function getInfoByUserid($userid){
			return $this->where(array('userid'=>$userid))->find();
		}

		public function getProgress($id){
			// 通过用户id得到用户资料完整度
			$info = $this->where(array('id'=>$id))->find();
			$i = 0;
			$sum = 0;
			foreach($info as $v){
				if($v != null)$i++;
				$sum++;
			}
			$res = $i*100/$sum;
			return $res;
		}
	}
?>