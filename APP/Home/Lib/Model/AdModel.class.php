<?php
	class AdModel extends Model{
		public function getAdBycategory($category){
			// 本站广告
			$data = $this->where(array('category'=>$category))->order('mshow desc')->select();
			$max = null;
			foreach($data as $v){
				if($max == null){
					$max = $v;
				}else{
					if($max['mshow']<$v['mshow']){
						$max = $v;
					}
				}
			}
			return $max;
			//$sql = 'select * from yigou_ad where show=(select max(show) from yigou_ad where category='.$category.')';
		}

		public function getLunbotuByCategory($category){
			// 轮播图广告
			$data = $this->where(array('category'=>$category))->order('mshow')->select();
			$res = array();
			foreach($data as $v){
				if($v['mshow']!=-1){
					$res[] = $v;
				}
			}
			return $res;
		}

		public function getwuxianByCategory($category){
			// 无限广告
			$data = $this->where(array('category'=>$category))->order('mshow desc')->select();
			$res = array();
			foreach($data as $v){
				if($v['mshow']!=-1){
					$res[] = $v;
				}
			}
			return $res;
		}
	}
?>