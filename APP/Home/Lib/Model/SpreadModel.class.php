<?php
	class SpreadModel extends Model{
		public function getAllData(){
			return $this->select();
		}

		public function getUserDataById($id){
			return $this->where(array('fromuser'=>$id))->select();
		}

		public function getUserDataCountById($id){
			$data = $this->getUserDataById($id);
			$result = count($data);
			return $result;
		}

		public function addData($fromuser, $touser){
			$data = array();
			$data['fromuser'] = $fromuser;
			$data['touser'] = $touser;
			$data['time'] = time();
			if(!$this->add($data))return false;
			return true;
		}
	}
?>