<?php
	class MessageModel extends Model{
		public function send($uid, $data, $msgtype){
			// $con = W('SendMessage', $data, true);
			// 将$data 转换成字符串
			$con = array_to_string($data);
			$tmp = array();
			$tmp['uid'] = $uid;
			$tmp['mtime'] = time();
			$tmp['content'] = $con;
			$tmp['msgtype'] = $msgtype;
			$tmp['hasread'] = 0;
			if(!$this->add($tmp))return false;
			return true;
		}

		public function getUnread($uid){
			$res = $this->where(array('uid'=>$uid, 'hasread'=>0))->order('mtime desc')->select();
			return $res;
		}

		public function getMessage($id){
			$res = $this->where(array('id'=>$id))->find();
			if($res!=null){
				$this->where(array('id'=>$id))->setField(array('hasread'=>1));
			}
			return $res;
		}
	}
?>