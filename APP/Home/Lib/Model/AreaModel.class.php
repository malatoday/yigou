<?php
	class AreaModel extends Model{
		public function getDataById($id){
			return $this->where(array('id'=>$id))->find();
		}

		public function getAreaProvinceById($id){
			return $this->where(array('id'=>$id))->getField('province');
		}

		public function getAreaCityById($id){
			return $this->where(array('id'=>$id))->getField('city');
		}

		public function getAreaAreaById($id){
			return $this->where(array('id'=>$id))->getField('area');
		}

		public function getAreaMoreById($id){
			return $this->where(array('id'=>$id))->getField('more');
		}

		public function addAreaInfo($data){
			return $this->add($data);
		}
	}
?>