<?php
	class MyegAction extends HomeAction{
		public function __construct(){
			parent::__construct();
			$this->assign('Myeg', 'selected');
		}

		public function index(){
			$User = D('User');
			$Points = D('Points');
			$Voucher = M('Voucher');
			$Coupon = M('Coupon');
			$Message = M('Message');
			$id = $User->getUserIdByUserid(session('userid'));
			$point = $Points->getNowPointsByUserId($id);
			if($point==NULL)$point=0;
			$data = $User->getInfoByUserid(session('userid'));
			$progress = $User->getProgress($id);
			$message = $Message->where(array('uid'=>$id))->order('mtime desc')->limit('2')->select();
			$countofcoupon = count($Coupon->where(array('uid'=>$id))->select());
			$countofvoucher = count($Voucher->where(array('uid'=>$id))->select());
			// 优惠券和代金券数据获取
			// 所有的申请
			$all1 = $Voucher->where(array('uid'=>$id, 'used'=>0))->order('mtime desc')->limit('5')->select();
			$all2 = $Coupon->where(array('uid'=>$id, 'used'=>0))->order('mtime desc')->limit('5')->select();
			$used1 = $Voucher->where(array('uid'=>$id, 'used'=>1))->order('mtime desc')->select();
			$used2 = $Coupon->where(array('uid'=>$id, 'used'=>1))->order('mtime desc')->select();
			$used1 = $this->addrow($used1, 'mtype', 'voucher');
			$used2 = $this->addrow($used2, 'mtype', 'coupon');
			//$all = $this->convert($all1, $all2);
			$all = $all2;
			$used = $this->convert($used1, $used2);
			$this->assign('all', $all);
			$this->assign('used', $used);
			$this->assign('data', $data);
			$this->assign('point', $point);
			$this->assign('progress', $progress);
			$this->assign('message', $message);
			$this->assign('countofcoupon', $countofcoupon);
			$this->assign('countofvoucher', $countofvoucher);
			$this->display();
		}

		public function addrow($arr, $name, $value){
			$res = array();
			foreach($arr as $v){
				$v[$name] = $value;
				$res[] = $v;
			}
			return $res;
		}

		public function convert($data1, $data2){
			$result = array();
			$i = 0;
			$j = 0;
			if(count($data1)==0)return $data2;
			if(count($data2)==0)return $data1;
			while($i<=(count($data1)-1) && $j<=(count($data2)-1)){
				if((int)$data1[$i]['myorder']>=(int)$data2[$j]['myorder']){
					$result[] = $data1[$i];
					$i++;
				}else{
					$result[] = $data2[$j];
					$j++;
				}
			}
			if($i==count($data1)){
				while($j<=(count($data2)-1)){
					$result[] = $data2[$j];
					$j++;
				}
			}
			if($j==(count($data2))){
				while($i<=count($data1)-1){
					$result[] = $data1[$i];
					$i++;
				}
			}
			return $result;
		}

		public function spread(){
			$Spread = D('Spread');
			$User = D('User');
			$data = array();
			$data = $Spread->getUserDataById($User->getUserIdByUserid(session('userid')));
			$res = array();
			foreach ($data as $v){
				$name = $User->where(array('id'=>$v['touser']))->getField('nick');
				$temp['name'] = $name;
				$temp['id'] = $v['touser'];
				$temp['time'] = $v['time'];
				$res[] = $temp;
			}
			$count = $Spread->getUserDataCountById($User->getUserIdByUserid(session('userid')));
			$jifen = $count*C('Spread_rate');
			$this->assign('spreadclass', 'selected');
			$this->assign('jifen', $jifen);
			$this->assign('count', $count);
			$this->assign('data', $res);
			$this->display();
		}

		public function coupon(){
			$Voucher = M('Voucher');
			$Coupon = M('Coupon');
			$Lifeserve = D('Lifeserve');
			$Loanmessage = D('Loanmessage');
			$User = D('User');
			$Points = D('Points');
			$uid = $User->getUserIdByUserid(session('userid'));
			$point = $Points->getNowPointsByUserId($uid);
			$vouchers = $Voucher->where(array('uid'=>$uid))->order('used, mtime desc')->select();
			$coupons = $Coupon->where(array('uid'=>$uid))->order('used, mtime desc')->select();
			$voucher = array();
			foreach($vouchers as $v){
				if($v['type']==C('Lifeserve')){
					$temp = $Lifeserve->getDataById($v['gid']);
					$v['area'] = $temp['area'];
				}else{
					$temp = $Loanmessage->getDataById($v['gid']);
					$v['area'] = $temp['area'];
				}
				$voucher[] = $v;
			}
			$coupon = array();
			foreach($coupons as $v){
				if($v['type']==C('Lifeserve')){
					$temp = $Lifeserve->getDataById($v['gid']);
					$v['area'] = $temp['area'];
				}else{
					$temp = $Loanmessage->getDataById($v['gid']);
					$v['area'] = $temp['area'];
				}
				$coupon[] = $v;
			}
			$this->assign('couponclass', 'selected');
			$this->assign('coupon', $coupon);
			$this->assign('voucher', $voucher);
			$this->assign('point', $point);
			$this->assign();
			$this->display();
		}

		public function points(){
			$Points = D('Points');
			$User = D('User');
			$id = $User->getUserIdByUserid(session('userid'));
			$total = $Points->where(array('user'=>$id))->getField('total');
			$used = $Points->where(array('user'=>$id))->getField('used');
			$now = $Points->getNowPointsByUserId($id);
			if($total == NULL)$total=0;
			if($used == NULL)$used=0;
			if($now == NULL)$now=0;
			$this->assign('pointsclass', 'selected');
			$this->assign('total', $total);
			$this->assign('used', $used);
			$this->assign('now', $now);
			$this->display();
		}

		public function mycollect(){
			$Collect = M('Collect');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			$data = $Collect->where(array('uid'=>$uid))->order('id desc')->select();
			$tmp = $data;
			$data = null;
			foreach($tmp as $v){
				$vo = getDataByTypeId($v['type'],$v['good_id']);
				if($vo['limit_time']>time()){
					$data[] = $v;
				}
			}
			$this->assign('data', $data);
			$this->display();
		}

		public function removePills(){
			$mtype = $this->_param('mtype');
			$id = $this->_param('id');
			if($mtype == 'voucher'){
				$DB = D('Voucher');
			}elseif($mtype == 'coupon'){
				$DB = D('Coupon');
			}
			if($DB->where(array('id'=>$id))->delete()===false){
				$this->error('删除失败');
			}else{
				$this->success('删除成功');
			}
		}
	}
?>