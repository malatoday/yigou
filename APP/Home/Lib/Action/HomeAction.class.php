<?php
	/**
	 * @author Parallel(mao@malatoday.com)
	 * @time 2015-3-13
	 * 这个类是前台程序的action基类，所有的Action类几乎都要继承本类
	*/
	class HomeAction extends Action{
		/**
		 * @author 
		 * @time
		 * @param 
		 * @return 
		 * 
		*/
		public function __construct(){
			header("Content-type:text/html;charset=utf-8");
			if(empty($_SESSION['region_id']))$_SESSION['region_id'] = 1;
			if(!isLogin()){
				$this->redirect('Public/login');
			}
			parent::__construct();
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			checkVoucher($uid);
		}
	}
?>