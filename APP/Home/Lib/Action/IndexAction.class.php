<?php

class IndexAction extends Action {
	public function __construct(){
		header("Content-type:text/html;charset=utf-8");
		if(empty($_SESSION['region_id']))$_SESSION['region_id'] = 1;trace($_SESSION);
		parent::__construct();
	}

	public function index(){
		$Loan = D('Loanmessage');
		$Life = D('Lifeserve');
		$data1 = $Loan->getData(C('Category_index'));
		$data2 = $Life->getData(C('Category_index'));
		$data3 = $Loan->getData(C('Category_shanGoLife'));
		$data4 = $Life->getData(C('Category_shanGoLife'));
		$data5 = $Loan->getData(C('Category_superShanGo'));
		$data6 = $Life->getData(C('Category_superShanGo'));
		$data7 = $Loan->getData(C('Category_qinsongYidai'));
		$data8 = $Life->getData(C('Category_qinsongYidai'));
		$region = $this->_param('region');
		$mdata1 = $this->convert($data1, $data2);
		$mdata2 = $this->convert($data3, $data4);
		$mdata3 = $this->convert($data5, $data6);
		$mdata4 = $this->convert($data7, $data8);
		$mdata5 = $this->convert($mdata1, $mdata2);
		$mdata6 = $this->convert($mdata3, $mdata4);
		$data = $this->convert($mdata5, $mdata6);
		$temp = array();
		foreach($data as $v){
			if($v['index_order']!=0){
				$temp[] = $v;
			}
		}
		$data = $temp;
		if(!empty($region)){
			foreach($data as $v){
				if($v['region']==$this->_param('region'))$tmp[] = $v;
			}
			$data = $tmp;
			$this->assign('region', $this->_param('region'));
		}
		function my_sort($a, $b){
			if($a['index_order']==$b['index_order'])return 0;
			return ($a['index_order']>$b['index_order'])?-1:1;
		}
		usort($data, "my_sort");
		$this->assign('data', $data);
		$content = W('ShowLunbotu', array('type'=>2), true);
		$this->assign('lunbotu', $content);
		$this->assign('index', 'selected');
    	$this->display();
	}

	public function test(){
		$this->display();

	}

	public function blinkEG(){
		// 闪GO会
		$Loan = D('Loanmessage');
		$Life = D('Lifeserve');/**/
		$data1 = $Loan->getData(C('Category_shanGoLife'));
		$data2 = $Life->getData(C('Category_shanGoLife'));
		$data = $this->convert($data1, $data2);
		$region = $this->_param('region');
		if(!empty($region)){
			foreach($data as $v){
				if($v['region']==$this->_param('region'))$tmp[] = $v;
			}
			$data = $tmp;
			$this->assign('region', $this->_param('region'));
		}
		$this->assign('data', $data);
		$content = W('ShowLunbotu', array('type'=>3), true);
		$this->assign('lunbotu', $content);
		$this->assign('blinkEG', 'selected');
    	$this->display();
	}

	public function superEG(){
		// 超级易GO
		$Loan = D('Loanmessage');
		$Life = D('Lifeserve');/**/
		$data1 = $Loan->getData(C('Category_superShanGo'));
		$data2 = $Life->getData(C('Category_superShanGo'));
		$data = $this->convert($data1, $data2);
		$region = $this->_param('region');
		if(!empty($region)){
			foreach($data as $v){
				if($v['region']==$this->_param('region'))$tmp[] = $v;
			}
			$data = $tmp;
			$this->assign('region', $this->_param('region'));
		}
		$this->assign('data', $data);
		$content = W('ShowLunbotu', array('type'=>4), true);
		$this->assign('lunbotu', $content);
		$this->assign('superEG', 'selected');
    	$this->display();
	}

	public function edh(){
		// 易贷会
		$Loan = D('Loanmessage');
		$Life = D('Lifeserve');/**/
		$data1 = $Loan->getData(C('Category_qinsongYidai'));
		$data2 = $Life->getData(C('Category_qinsongYidai'));
		$data = $this->convert($data1, $data2);
		$region = $this->_param('region');
		if(!empty($region)){
			foreach($data as $v){
				if($v['region']==$this->_param('region'))$tmp[] = $v;
			}
			$data = $tmp;
			$this->assign('region', $this->_param('region'));
		}
		$this->assign('data', $data);
		$content = W('ShowLunbotu', array('type'=>5), true);
		$this->assign('lunbotu', $content);
		$this->assign('edh', 'selected');
    	$this->display();
	}

	/**
	 * @author Parallel(mao@malatoday.com)
	 * @time 2015-2-22
	 * @param $data1 -- 二维数组1    $data2 -- 二维数组2
	 * @return $data  组合后的二维数组
	 * 本函数的功能是将参数传递过来的两个有序二维数组合并排序为一个二维数组并返回结果
	*/
	private function convert($data1, $data2){
		$result = array();
		$i = 0;
		$j = 0;
		if(count($data1)==0)return $data2;
		if(count($data2)==0)return $data1;
		while($i<=(count($data1)-1) && $j<=(count($data2)-1)){
			if((int)$data1[$i]['myorder']>=(int)$data2[$j]['myorder']){
				$result[] = $data1[$i];
				$i++;
			}else{
				$result[] = $data2[$j];
				$j++;
			}
		}
		if($i==count($data1)){
			while($j<=(count($data2)-1)){
				$result[] = $data2[$j];
				$j++;
			}
		}
		if($j==(count($data2))){
			while($i<=count($data1)-1){
				$result[] = $data1[$i];
				$i++;
			}
		}
		return $result;
	}

	private function get_State($url,$request){
	    $output=true;
	    $show_header=false;
	    $ch=curl_init();
	    curl_setopt($ch, CURLOPT_URL,$url);
	    curl_setopt($ch, CURLOPT_POSTFIELDS,$request);    //发送的数据
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER,$output); //设定返回的数据是否自动显示
	    curl_setopt($ch, CURLOPT_HEADER,$show_header);    //设定是否显示头信息
	    $ReturnData = curl_exec($ch);
	    curl_close($ch);
	    return $ReturnData;
	}

	private function xml_to_array($xml){
		$reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
		if(preg_match_all($reg, $xml, $matches)){
			$count = count($matches[0]);
			for($i = 0; $i < $count; $i++){
			$subxml= $matches[2][$i];
			$key = $matches[1][$i];
				if(preg_match( $reg, $subxml )){
					$arr[$key] = xml_to_array( $subxml );
				}else{
					$arr[$key] = $subxml;
				}
			}
		}
		return $arr;
	}

	private function myXmlToArray($str){
        $xml = simplexml_load_string($str);
        $data = array();
        foreach($xml->children() as $v){
                $data[$v->getName()] = $v;
        }
        return $data;
	}

	private function random($length = 6 , $numeric = 0) {
		PHP_VERSION < '4.2.0' && mt_srand((double)microtime() * 1000000);
		if($numeric) {
			$hash = sprintf('%0'.$length.'d', mt_rand(0, pow(10, $length) - 1));
		} else {
			$hash = '';
			$chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789abcdefghjkmnpqrstuvwxyz';
			$max = strlen($chars) - 1;
			for($i = 0; $i < $length; $i++) {
				$hash .= $chars[mt_rand(0, $max)];
			}
		}
		return $hash;
	}

	public function sendVerifyCode(){
		$sn=C('DX_sn');
		$pwd=C('DX_pwd');
		$mobile = $_POST['mobile'];

		$send_code = $_POST['send_code'];
		if(empty($mobile)){
			exit('手机号码不能为空');
		}
		// $this->ajaxReturn($_SESSION['send_code'], 'aa', 1);
		// if(empty($send_code) or $send_code!=session('send_code')){
		// 	//防用户恶意请求
		// 	$this->ajaxReturn(session('send_code'), 'aa', 1);
		// 	//exit('请求超时，请刷新页面后重试');
		// }
		$mobile_code = $this->random(6,1);
		$content='您好。您的验证码为'.$mobile_code.'。请及时操作！【易GO】';
		$ext='';
		$rrid='';
		$stime='';
		$stype=1;
		$ssafe=2;
		$scode=1;
		$MARK = "|";
		$Md5key=C('DX_md5sign');//md5密钥（KEY）
		//$Md5Sign=md5($sn.$MARK.$pwd.$MARK.$mobile.$MARK.$content.$MARK.$ext.$MARK.$rrid.$MARK.$stime.$MARK.$stype.$MARK.$ssafe.$MARK.$scode.$MARK.$Md5key);
		$target = "http://sdk8.interface.sudas.cn/z_mdsmssend.php";
		$subdate="sn=".$sn."&pwd=".$pwd."&mobile=".$mobile."&content=".urlencode($content)."&ext=".$ext."&stime=".$stime."&rrid=".$rrid."&stype=".$stype."&ssafe=".$ssafe."&scode=".$scode;
		$sendMsgID=$this->get_State($target,$subdate);
		if($sendMsgID > 1){
			session('mobile', $mobile);
			session('mobile_code', $mobile_code);
			$this->ajaxReturn($sendMsgID,"新增成功！",1);
		}else{
			$this->ajaxReturn($sendMsgID,"新增失败！",0);
		}
	}

	 public function setSession(){
	 	$send_code = $this->_post('send_code');
	// 	session('send_code', $send_code);
	 	$this->ajaxReturn($send_code,"新增成功！",1);
	 }

	public function jump(){
		$id = $this->_param('id');
		$name = $this->_param('name');
		$Region = D('Region');
		if($Region->where(array('id'=>$id))->getField('name')==$name){
			session('region_id', $id);
			session('region_name', $name);
			cookie('region_name', $name);
			cookie('region_id', $id);
		}
		$this->redirect('Index/index');
	}

	public function search(){
		// 获取关键字
		$key = $this->_param('key');
		$mkeys = explode(' ', $key);
		$keys = array();
		$map = array();
		foreach($mkeys as $v){
			if($v!='')$keys[] = '%'.$v.'%';
		}
		// 检索地区
		$Area = M('Area');
		$areamap1['province'] = array('like', $keys, 'OR');
		//$areamap2['city'] = array('like', $keys, 'OR');
		$areamap3['area'] = array('like', $keys, 'OR');
		//$areamap4['more'] = array('like', $keys, 'OR');
		$areadata = $Area->where($areamap1)->select();
		foreach($areadata as $v){
			$mareadata[] = $v['id'];
		}
		//$mareadata = 
		if($areadata!=null){
			$map['area'] = array('in', $mareadata);
		}else{
			$areadata = $Area->where($areamap3)->select();
			foreach($areadata as $v){
				$mareadata[] = $v['id'];
			}
			if($areadata!=null){
				$map['area'] = array('in', $mareadata);
			}
		}
		$Lifeserve = M('Lifeserve');
		$Loanmessage = M('Loanmessage');
		if(count($keys)==1 && !empty($map['area'])){
			//$map['company_name'] = array('like', $keys, 'OR');
			$data1 = $Lifeserve->where($map)->select();
			$data2 = $Loanmessage->where($map)->select();
		}else{
			$map1['company_name'] = array('like', $keys, 'OR');
			$data1 = $Lifeserve->where($map1)->select();
			$data2 = $Loanmessage->where($map1)->select();
		}
		//$map2['ad_title'] = array('like', '%'.$key.'%');
		$data = $this->convert($data1, $data2);
		// 过滤日期
		$res = array();
		foreach($data as $v){
			if($v['limit_time']>time() || $v['limit_time']==0){
				if($v['index_order']!=0){
					$res[] = $v;
				}
			}
		}
		trace($map);
		trace($mareadata);
		$this->assign('data', $res);
		$this->display();
	}
}