<?php
	class MyAccountAction extends HomeAction{
		public function __construct(){
			parent::__construct();
			$this->assign('MyAccount', 'selected');
		}

		public function index(){
			$this->redirect('MyAccount/myinfo', '跳转到我的账号', 0);
		}

		public function myinfo(){
			$User = D('User');
			$Loaninfo = D('Loaninfo');
			$data = $Loaninfo->getInfoByUserid(session('userid'));
			$info = $User->getInfoByUserid(session('userid'));
			$id = $User->getUserIdByUserid(session('userid'));
			$progress = $User->getProgress($id);
			$this->assign('myinfoclass', 'selected');
			$this->assign('progress', $progress);
			$this->assign('data', $data);
			$this->assign('info', $info);
			$this->display();
		}

		public function safeCenter(){
			$this->assign('safeCenterclass', 'selected');
			$this->display();
		}

		public function changePasswd(){
			$oldpasswd = $this->_param('oldpasswd');
			$newpasswd = $this->_param('newpasswd');
			$repeatpasswd = $this->_param('repeatpasswd');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			if($newpasswd!=$repeatpasswd)$this->error('两次输入的密码不一致');
			if($User->where(array('id'=>$uid))->getField('password')!=md5($oldpasswd)){
				$this->error('你输入的旧密码是错误的');
			}
			if($User->where(array('id'=>$uid))->setField('password', md5($newpasswd))===false){
				$this->error('密码修改失败');
			}
			$this->success('密码修改成功');
		}

		public function changePhone(){
			if(session('mobile_code')!=$this->_post('code'))$this->error('短信验证码错误');
			$phonenumber = $this->_param('phonenumber');
			$User = D('User');
			if($User->where(array('username'=>$phonenumber))->find()!=null)$this->error('该手机号已经被注册');
			$uid = $User->getUserIdByUserid(session('userid'));
			if($User->where(array('id'=>$uid))->setField('username', $phonenumber)===false){
				$this->error('修改失败');
			}
			// 发送站内信
			$Message = D('Message');
			$msg = array();
			$Message->send($uid, $msg, C('MSG_CHANGEPHONE'));
			$this->success('修改成功');
		}

		public function baseinfo(){
			$User = D('User');
			$Area = D('Area');
			$info = $User->getInfoByUserid(session('userid'));
			$data = $_POST;
			/** 检测用户输入的数据合法性 */
			if($data['nick']=='')$data['nick'] = $info['nick'];
			if($data['mobile']=='')$data['mobile'] = $info['mobile'];
			if($data['telpre']=='')$data['telpre'] = $info['telpre'];
			if($data['telcon']=='')$data['telcon'] = $info['telcon'];
			if($data['mail']=='')$data['mail'] = $info['mail'];
			if($data['alipayaccount']=='')$data['alipayaccount'] = $info['alipayaccount'];
			if($data['alipayname']=='')$data['alipayname'] = $info['alipayname'];
			$data['id'] = $info['id'];
			$data['userid'] = $info['userid'];
			$area = array();
			$area['province'] = $this->_post('province');
			$area['city'] = $this->_post('city');
			$area['area'] = $this->_post('area');
			$area['more'] = $this->_post('more');
			if($info['area']==null){
				$areaid = $Area->add($area);
				if(!$areaid)$this->error('地址添加失败');
			}else{
				$area['id'] = $info['area'];
				if($Area->save($area)===false)$this->error('地址更新失败');
				$areaid = $info['area'];
			}
			$data['area'] = $areaid;
			$data['photo'] = $info['photo'];
			$data['username'] = $info['username'];
			$data['password'] = $info['password'];
			if($User->save($data)===false)$this->error('信息更新失败');
			// 更新session
			session('nick', $data['nick']);
			$this->success('更新成功', U('MyAccount/myinfo'));
		}

		public function loaninfo(){
			$Loaninfo = D('Loaninfo');
			$User = D('User');
			$Area = D('Area');
			$info = $Loaninfo->getInfoByUserid(session('userid'));
			if($info==null){
				// 新增记录
				$data = array();
				$data['uid'] =	$User->getUserIdByUserid(session('userid'));
				$data['userid'] = session('userid');
				$data['name'] = $this->_post('name');
				$data['type'] = $this->_post('type');
				$area = array();
				$area['province'] = $this->_post('province');
				$area['city'] = $this->_post('city');
				$area['area'] = $this->_post('area');
				$area['more'] = $this->_post('more');
				$areaid = $Area->addAreaInfo($area);
				$data['area'] = $areaid;
				$data['mobile'] = $this->_post('mobile');
				if(!$Loaninfo->add($data))$this->error('设置失败');
			}else{
				// 更新记录
				$data = $info;
				$area['province'] = $this->_post('province');
				$area['city'] = $this->_post('city');
				$area['area'] = $this->_post('area');
				$area['more'] = $this->_post('more');
				$area['id'] = $info['area'];
				if($Area->save($area)===false)$this->error('地址更新失败');
				$areaid = $info['area'];
				$data['name'] = $this->_post('name')=='' ? $info['name'] : $this->_post('name');
				$data['type'] = $this->_post('type');
				$data['mobile'] = $this->_post('mobile')=='' ? $info['mobile'] : $this->_post('mobile');
				if($Loaninfo->save($data)===false)$this->error('设置失败');
			}
			$this->success('设置成功', U('MyAccount/myinfo'));
		}

		public function changePhoto(){
			import('ORG.Net.UploadFile');
			$upload = new UploadFile();
			$upload->maxSize = 3145728;
			$upload->allowExts  = array('jpg', 'gif', 'png', 'jpeg');
			$upload->savePath = 'Public/pic/';
			if(!$upload->upload()){
				$this->error($upload->getErrorMsg());
			}else{
				$info = $upload->getUploadFileInfo();
			}
			$User = D('User');
			$Pic = D('Pic');
			$UserInfo = $User->getInfoByUserid(session('userid'));
			$data = array();
			$data['savepath'] = $info[0]['savepath'];
			$data['name'] = $info[0]['name'];
			$data['savename'] = $info[0]['savename'];
			$data['size'] = $info[0]['size'];
			$data['type'] = $info[0]['type'];
			$data['extension'] = $info[0]['extension'];
			if($UserInfo['photo']!=null){
				// 更新数据
				$data['id'] = $UserInfo['photo'];
				if($Pic->save($data)===false)$this->error('图片保存失败');
			}else{
				// 新增数据
				$photo = $Pic->add($data);
				if(!$photo)$this->error('图片保存失败');
			}
			$User->where(array('id'=>$UserInfo['id']))->setField('photo', $photo);
			$this->success('头像修改成功', U('MyAccount/myinfo'));
		}
	}
?>