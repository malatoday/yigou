<?php
	class MemberAction extends HomeAction{
		public function showInfo(){
			$User = D('User');
			$Pic = D('Pic');
			$Info = $User->where(array('name'=>session('name')))->find();
			$data = array();
			if($Info['photo']){
				$data = $Pic->getInfoById($Info['photo']);
			}else{
				$data = $Pic->getDefaultPhoto();
			}
			if(!$Info['name'])$Info['name'] = '未完善';
			if(!$Info['tel'])$Info['tel'] = '未完善';
			if(!$Info['qq'])$Info['qq'] = '未完善';
			if(!$Info['email'])$Info['email'] = '未完善';
			if(!$Info['area'])$Info['area'] = '未完善';
			$this->assign('UserInfo', $Info);
			$this->assign('photoInfo', $data);
			$this->display();
		}

		public function changePhoto(){
			
		}

		public function changeTel(){

		}

		public function changeQQ(){

		}

		public function changeEmail(){

		}

		public function changeArea(){

		}
		
		/**
		 * @param
		 * @return
		 * 完善个人信息
		*/
		public function prefectPeopleInfo(){
			$this->display();
		}
	}
?>