<?php
	/**
	 * @author Parallel(mao@cloudkz.cn)
	 * @time 2015-3-1
	 * 详细页面类
	*/
	class DetailsAction extends HomeAction{
		public function show(){
			$type = $this->_param('type');
			$id = $this->_param('id');
			$Details = M('Details');
			$Points = D('Points');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			$point = $Points->getNowPointsByUserId($uid);
			$data = $Details->where(array('type'=>$type, 'lid'=>$id))->find();
			if($data==null){
				$this->error('本商品暂无详细页面，请稍后访问');
			}else{
				if($type==C('Lifeserve')){
					$Lifeserve = M('Lifeserve');
					$tmp = $Lifeserve->where(array('id'=>$id))->find();
					if((int)$tmp['limit_time']<=time()){
						$this->error('本商品已过期');
					}
				}else{
					$Loanmessage = M('Loanmessage');
					$tmp = $Loanmessage->where(array('id'=>$id))->find();
					if((int)$tmp['limit_time']<=time()){
						$this->error('本商品已过期');
					}
				}
			}
			$this->assign('data', $data);
			$this->assign('point', $point);
			$this->display();
		}
	}
?>