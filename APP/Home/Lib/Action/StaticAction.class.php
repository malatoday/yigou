<?php
	class StaticAction extends Action{
		public function aboutUs(){
			// 关于我们
			$this->display();
		}

		public function contactUs(){
			// 联系我们
			$this->display();
		}

		public function brandInvestment(){
			// 品牌招商
			$this->display();
		}

		public function privacy(){
			// 隐私条款
			$this->display();
		}

		public function userExperienceProject(){
			// 用户体验计划
			$this->display();
		}

		public function help(){
			// 帮助中心
			$this->display();
		}
	}
?>