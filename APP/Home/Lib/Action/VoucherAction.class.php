<?php
	/**
	 * @auther Parallel(mao@cloudkz.cn)
	 * @time 2015-3-3
	 * 代金券类
	*/
	class VoucherAction extends HomeAction{
		public function draw(){
			$value = $this->_param('value');
			$type = $this->_param('type');
			$gid = $this->_param('gid');
			$Lifeserve = D('Lifeserve');
			$Loanmessage = D('Loanmessage');
			$User = D('User');
			$Voucher = M('Voucher');
			$uid = $User->getUserIdByUserid(session('userid'));
			$Points = D('Points');
			$point = $Points->getNowPointsByUserId($uid);
			if($Voucher->where(array('uid'=>$uid, 'type'=>$type, 'gid'=>$gid, 'used'=>0))->find()!=null)$this->error('无法重复领取代金券');
			if($point===false)$this->error('系统错误');

			// 检测用户输入的积分是否到达最低积分
			if($value < C('MIN_POINTS'))$this->error('无法兑换小余'.C('MIN_POINTS').'的积分');
			if($value > $point)$this->error('积分余额不足');

			// 计算出对应的yuan
			if($value%100!=0)$this->error('积分必须使用100的整数倍');
			$yuan = (float)$value / C('POINTS_TO_YUAN');

			// 先扣除用户对应的积分，再新增代金券，如果代金券新增失败而返还用户积分
			if($Points->cutPoints($value, $uid)===false)$this->error('兑换失败');
			
			$Details = M('Details');
			$sid = $Details->where(array('lid'=>$gid, 'type'=>$type))->getField('seller');
			$data['uid'] = $uid;
			$data['type'] = $type;
			$data['gid'] = $gid;
			$data['sid'] = $sid;
			$data['value_yuan'] = $yuan;
			$data['mtime'] = time();
			$Details = M('Details');
			$temp = array();
			if($type == C('Lifeserve')){
				$temp = $Lifeserve->getDataById($gid);
			}else{
				$temp = $Loanmessage->getDataById($gid);
			}
			$data['limit_time'] = $temp['limit_time'];
			$data['used'] = 0;
			if(!$Voucher->add($data)){
				// 新增代金券失败，返还用户积分后输出错误信息
				$resss = $Points->returnPoints($value, $uid);
				$this->error('兑换失败');
			}
			$this->success('成功兑换');
		}

		public function edit(){
			$id = $this->_param('id');
			$value = $this->_param('value');
			$Voucher = M('Voucher');
			$Points = D('Points');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			$nowPoints = $Points->getNowPointsByUserId($uid);
			$info = $Voucher->where(array('id'=>$id, 'used'=>0))->find();
			if($info['limit_time']<=time()){
				$this->error('已过截至日期！');
			}
			if((int)$info['value_yuan']*C('POINTS_TO_YUAN')+(int)$nowPoints < (int)$value){
				// 积分不足以兑换
				$this->error('您的积分不足以兑换');
			}
			if($Voucher->where(array('id'=>$id))->setField('value_yuan', ((int)$value/C('POINTS_TO_YUAN')))){
				// 代金券修改成功
				// 先返还积分
				$Points->returnPoints((int)$info['value_yuan']*C('POINTS_TO_YUAN'), $uid);
				// 再扣除积分
				$Points->cutPoints((int)$value, $uid);
				$this->success('代金券修改成功');
			}
			// 至此，代金券修改失败
			$this->error('代金券修改失败');
		}

		public function delete(){
			$id = $this->_param('id');
			$Voucher = M('Voucher');
			$Points = D('Points');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			$info = $Voucher->where(array('id'=>$id, 'used'=>0))->find();
			if($info==null){
				$this->error('本信息已不存在');
			}
			if($Voucher->where(array('id'=>$id))->delete()){
				// 代金券删除成功
				// 返还积分
				$Points->returnPoints((int)$info['value_yuan']*C('POINTS_TO_YUAN'), $uid);
				$this->success('代金券删除成功', U('Myeg/coupon'));
			}
			$this->error('代金券删除失败');
		}

		public function remove(){
			$id = $this->_param('id');
			$Voucher = M('Voucher');
			$User = D('User');
			$uid = $User->getUserIdByUserid(session('userid'));
			$info = $Voucher->where(array('id'=>$id, 'used'=>1))->find();
			if($info==null){
				$this->error('本信息已不存在');
			}
			if($Voucher->where(array('id'=>$id))->delete()){
				// 代金券信息删除成功
				$this->success('代金券删除成功', U('Myeg/coupon'));
			}
			$this->error('代金券删除失败');
		}
	}
?>