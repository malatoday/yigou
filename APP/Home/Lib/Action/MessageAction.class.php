<?php
	class MessageAction extends HomeAction{
		public function __construct(){
			parent::__construct();
			$this->assign('Message', 'selected');
		}

		public function index(){
			$this->redirect('Message/unread', '跳转到我的账号', 0);
		}

		public function all(){
			$Message = D('Message');
			$User = D('User');
			$uid = $User->where(array('userid'=>session('userid')))->getField('id');
			$data = $Message->where(array('uid'=>$uid))->order('mtime desc')->select();
			$this->assign('allclass', 'selected');
			$this->assign('data', $data);
			$this->display();
		}

		public function unread(){
			$Message = D('Message');
			$User = D('User');
			$uid = $User->where(array('userid'=>session('userid')))->getField('id');
			$data = $Message->getUnread($uid);
			$this->assign('unreadclass', 'selected');
			$this->assign('data', $data);
			$this->display();
		}

		public function show(){
			$id = $this->_param('id');
			$Message = D('Message');
			$data = $Message->getMessage($id);
			$arr = array();
			if($data['msgtype']==C('MSG_ADDPOINT')){
				$arr = string_to_array($data['content']);
				$data['area'] = $arr[0];
				$data['discount'] = $arr[1];
				$data['suf'] = $arr[2];
				$data['usedpoints'] = $arr[3];
			}
			$this->assign('data', $data);
			$this->display();
		}
	}
?>