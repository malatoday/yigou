<?php
	/**
	 * @author Parallel(mao@cloudkz.cn)
	 * @time 2015-3-3
	 * 收藏类
	*/
	class CollectAction extends HomeAction{
		public function addcol(){
			$User = D('User');
			$type = $this->_param('type');
			$id = $this->_param('id');
			$uid = $User->getIdByUsername(session('username'));
			$data['uid'] = $uid;
			$data['type'] = $type;
			$data['good_id'] = $id;
			$Collect = M('Collect');
			if($Collect->where(array('uid'=>$uid, 'type'=>$type, 'good_id'=>$id))->find()!=null)$this->error('你已经收藏了');
			$data['time'] =  time();
			if(!$Collect->add($data))$this->error('收藏失败');
			$this->success('收藏成功');
		}

		public function cancel(){
			$User = D('User');
			$id = $this->_param('id');
			$uid = $User->getIdByUsername(session('username'));
			$Collect = M('Collect');
			if($Collect->where(array('uid'=>$uid, 'id'=>$id))->delete()===false)$this->error('删除失败');
			$this->success('删除成功');
		}
	}
?>