<?php
	class PublicAction extends Action{
		public function login(){
			$this->display();
		}

		public function checkLogin(){
			if($this->_post('username')=='')$this->error('账号错误');
			if($this->_post('password')=='')$this->error('密码为空！');
			$map = array();
			$map['username'] = $this->_post('username');
			$User = D('User');
			$userInfo = $User->checkUser($map);
			if(empty($userInfo))$this->error('账号不存在或被禁用');
			if($userInfo['password']!=md5($this->_post('password')))$this->error('用户名或密码错误');
			session('nick', $userInfo['nick']);
			session('tel', $userInfo['tel']);
			session('username', $userInfo['username']);
			session('email', $userInfo['email']);
			session('area', $userInfo['area']);
			session('app_id', 'YIGOU_HOME');
			session('userid', $userInfo['userid']);
			if($this->_post('remeberme')==1)cookie('username', $userInfo['username'], 360000);
			//cookie('username', $userInfo['username']);
			$this->success('登陆成功' ,U('Index/index'), 0);
		}

		public function register(){
			$this->display();
		}

		public function registerHandle(){
			if($this->_post('username')=='')$this->error('用户名不可以为空');
			if($this->_post('password')!=$this->_post('repeat'))$this->error('两次输入的密码不一致');
			if(session('verify')!=md5($this->_post('code')))$this->error('验证码错误');
			if($_SESSION['mobile_code']!=$this->_post('phonecode'))$this->error('短信验证码错误');
			$User = D('User');
			$map = array();
			$map['username'] = $this->_post('username');
			if($User->checkUser($map))$this->error('用户名已存在');
			if(!$User->addUser($this->_post('username'), $this->_post('password')))$this->error('注册失败！');
			$id = $User->getIdByUsername($this->_post('username'));
			// 检测推广码是否有效
			$Points = D('Points');
			$Points->newPoints(0, $id); // 初始化积分
			if($this->_post('invitecode')!=''){
				$fromUser = $User->where(array('userid'=>$this->_post('invitecode')))->find();
				if($fromUser){
					// 推广码有效,开始添加推广积分
					$Spread = D('Spread');
					$point = C('Spread_rate');
					if($Spread->addData($fromUser['id'], $id)){
						if($Points->addPoints($point, $fromUser['id'])){
							if($Points->addPoints($point, $id)){
								// 推广码积分添加成功
							}
						}
						// $this->success('注册成功且您填写的推广码成功被系统识别', U('Public/login'));
					}
				}
			}
			// 发送站内信
			$Message = D('Message');
			$msg = array();
			if($Message->send($id ,$msg, C('MSG_RESGITER'))==false)$this->error('站内信发送失败');
			$userInfo = $User->where(array('id'=>$id))->find();
			session('nick', $userInfo['nick']);
			session('tel', $userInfo['tel']);
			session('username', $userInfo['username']);
			session('email', $userInfo['email']);
			session('area', $userInfo['area']);
			session('app_id', 'YIGOU_HOME');
			session('userid', $userInfo['userid']);
			cookie('username', $userInfo['username']);
			$this->success('注册成功！', U('Index/index'));
		}

		public function verify(){
			import("ORG.Util.Image");
			Image::buildImageVerify();
		}

		public function logout(){
			session(null);
			$this->success('Public/login');
		}

		public function findPasswd(){
			// 找回密码
			$this->display();
		}

		public function resetPasswd(){
			if(session('verify')!=md5($this->_post('code')))$this->error('验证码错误');
			if(session('mobile_code')!=$this->_post('phonecode'))$this->error('短信验证码错误');
			$password = $this->_post('password');
			$repeat = $this->_post('repeat');
			$username = $this->_post('username');
			if($password!=$repeat){
				$this->error('两次输入的密码不一致');
			}
			if($password=='')$this->error('密码不可为空');
			$User = D('User');
			$uid = $User->where(array('username'=>$username))->getField('id');
			if($uid!=null){
				if($User->where(array('id'=>$uid))->setField('password', md5($password))===false){
					$this->error('重置失败');
				}else{
					$this->success('重置成功', U('Public/login'));
				}
			}
		}

		public function checkHasRes(){
			$User = M('User');
			$mobile = $this->_post('mobile');
			if($User->where(array('username'=>$mobile))->find()){
				$this->ajaxReturn('手机号已经被使用',"检测成功！",0);
			}
			$this->ajaxReturn('手机号未被使用',"检测成功！",1);
		}

		public function checkInvitecode(){
			$code = $this->_post('invitecode');
			$data = array();
			$User = M('User');
			if($User->where(array('userid'=>$code))->find()!=null){
				$data = array();
				$data['status'] = 1;
				$this->ajaxReturn($data, '检测正确', 1);
			}
			$this->ajaxReturn($data, '检测错误', 0);
		}
	}
?>