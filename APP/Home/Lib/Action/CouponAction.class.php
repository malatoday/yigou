<?php
	/**
	 * @author Parallel(mao@cloudkz.cn)
	 * @time 2015-3-3
	 * 优惠券类
	*/
	class CouponAction extends HomeAction{
		public function draw(){
			$User = D('User');
			$Lifeserve = D('Lifeserve');
			$Loanmessage = D('Loanmessage');
			$type = $this->_param('type');
			$good_id = $this->_param('gid');
			$uid = $User->getIdByUsername(session('username'));
			$Coupon = D('Coupon');
			$res = $Coupon->where(array('type'=>$type, 'gid'=>$good_id, 'uid'=>$uid, 'used'=>0))->find();
			$Details = M('Details');
			$sid = $Details->where(array('lid'=>$good_id, 'type'=>$type))->getField('seller');
			$data['type'] = $type;
			$data['gid'] = $good_id;
			$data['uid'] = $uid;
			$data['sid'] = $sid;
			$data['used'] = 0;
			$data['mtime'] = time();
			if($type == C('Lifeserve')){
				$temp = $Lifeserve->getDataById($good_id);
			}else{
				$temp = $Loanmessage->getDataById($good_id);
			}
			$data['limit_time'] = $temp['limit_time'];
			$Details = M('Details');
			$temp = $Details->where(array('type'=>$type, 'lid'=>$good_id))->find();
			$data['discount'] = $temp['discount'];
			if($res == null){
				if($Coupon->add($data)===false)$this->error('领取失败');
				$this->success('领取成功');
			}else{
				$this->error('无法重复领取');
			}
		}

		public function remove(){
			$id = $this->_param('id');
			$Coupon = M('Coupon');
			if($Coupon->where(array('id'=>$id))->delete()===false)$this->error('请求失败');
			$this->success('取消成功');
		}
	}
?>