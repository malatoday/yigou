<?php
	class ShowAdWidget extends Widget{
		public function render($data){
			/**
			 * type=>10 本站广告-首页
			 * 		 11			-闪GO会
			 * 		 12			-超级闪GO
			 *		 13			-易贷会
			 *		 14-17	轮播图广告
			 *		 18-21	无限广告
			*/
			$Ad = D('Ad');
			$Pic = D('Pic');
			switch ($data['type']) {
				case 10:
				case 11:
				case 12:
				case 13:
					// 本站广告
					$res = $Ad->getAdBycategory($data['type']);
					if($res!=null){
						$result = array();
						$res['img'] = $Pic->getInfoById($res['pic']);
						if($this->checkRegion($res)){
							$result = $res;
						}
						if(!empty($result)){
							$data['data'] = $result;
							$content = $this->renderFile('benzhan', $data);
						}else{
							$content = '';
						}
					}else{
						$content = '';
					}
					break;
				case 14:
				case 15:
				case 16:
				case 17:
					// 轮播图广告
					$res = $Ad->getLunbotuByCategory($data['type']);
					if($res!=null){
						$result = array();
						foreach($res as $v){
							$v['img'] = $Pic->getInfoById($v['pic']);
							if($this->checkRegion($v)){
								$result[] = $v;
							}
						}
						if(!empty($result)){
							$data['data'] = $result;
							$content = $this->renderFile('lunbotu', $data);
						}else{
							$content = '';
						}
						
					}else{
						$content = '';
					}
					break;
				case 18:
				case 19:
				case 20:
				case 21:
					// 无限广告
					$res = $Ad->getwuxianByCategory($data['type']);
					if($res!=null){
						foreach($res as $v){
							$v['img'] = $Pic->getInfoById($v['pic']);
							if($this->checkRegion($v)){
								$result[] = $v;
							}
						}
						if(!empty($result)){
							$data['data'] = $result;
							$content = $this->renderFile('wuxian', $data);
						}else{
							$content = '';
						}
					}else{
						$content = '';
					}
					break;
				default:
					break;
			}
			return $content;
		}

		private function checkRegion($item){
			trace($_SESSION, 'session');
			$Region = M('Region');
			$pa = $Region->where(array('id'=>$item['region']))->getField('pid');
			if($item['region']!=session('region_id') &&  $pa!=session('region_id'))return false;
			return true;
		}
	}
?>