<?php
	class ShowRegionWidget extends Widget{
		public function render($data){
			$Region = D('Region');
			$data['data'] = $Region->getAllPids();
			$content = $this->renderFile('regionList', $data);
			return $content;
		}
	}
?>