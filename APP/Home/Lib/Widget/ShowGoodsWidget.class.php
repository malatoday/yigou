<?php
	class ShowGoodsWidget extends Widget{
		public function render($data){
			/**
			 * Id
			 * Type:
			 *		 0=>生活服务
			 *		 1=>贷款信息
			*/
			$Loan = D('Loanmessage');
			$Life = D('Lifeserve');
			$Pic = D('Pic');
			$Area = D('Area');
			$User = D('User');
			$Collect = M('Collect');
			$userid = session('userid');
			$uid = $User->getUserIdByUserid($userid);
			$like = $Collect->where(array('uid'=>$uid, 'type'=>$data['type'], 'good_id'=>$data['id']))->find();
			trace($uid);
			trace($data);
			if($like==null){
				$data['like'] = 0;
			}else{
				$data['like'] = 1;
			}
			if($data['type']==C('Lifeserve')){
				// 生活服务
				$res = $Life->getDataById($data['id']);
				if($res!=null){
					$res['logoimg'] = $Pic->where(array('id'=>$res['logo']))->find();
					$res['img'] = $Pic->where(array('id'=>$res['pic']))->find();
					$res['areainfo'] = $Area->getDataById($res['area']);
					$data['data'] = $res;
					$content = $this->renderFile('goodoflife', $data);
				}else{
					$content = '';
				}
			}elseif($data['type']==C('Loanmessage')){
				// 贷款信息
				$res = $Loan->getDataById($data['id']);
				if($res!=null){
					$res['logoimg'] = $Pic->where(array('id'=>$res['logo']))->find();
					$res['img'] = $Pic->where(array('id'=>$res['pic']))->find();
					$res['areainfo'] = $Area->getDataById($res['area']);
					$data['data'] = $res;
					$content = $this->renderFile('goodofloan', $data);
				}else{
					$content = '';
				}				
			}else{
				$content = '';
			}
			return $content;
		}
	}
?>