<?php
	class ShowLunbotuWidget extends Widget{
		public function render($data){
			/**
			 * Type =>	2,	//首页
			 * 			3,	//闪GO会
			 * 			4,	//超级闪GO
			 * 			5,	//易贷会
			 *---------------------------------------------*
			*/
			$res = array();
			$Lunbotu = D('Lunbotu');
			switch ($data['type']){
				case 2:
					$res = $Lunbotu->getDataByType(2);
					break;
				case 3:
					$res = $Lunbotu->getDataByType(3);
					break;
				case 4:
					$res = $Lunbotu->getDataByType(4);
					break;
				case 5:
					$res = $Lunbotu->getDataByType(5);
					break;
				default:
					$res = $Lunbotu->getDataByType(2);
					break;
			}
			if($res!=null){
				// 过滤地区
				$Region = D('Region');
				$Pic = D('Pic');
				$region_id = session('region_id')==null?3:session('region_id');
				$result = array();
				foreach($res as $v){
					if($v['region']==$region_id || $Region->getPidById($v['region'])==$region_id){
						$v['img'] = $Pic->where(array('id'=>$v['pic']))->find();
						$result[] = $v;
					}
				}
				$data['data'] = $result;
				$content = $this->renderFile('lunbotu', $data);
			}else{
				$content = '';
			}
			return $content;
		}
	}
?>